-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 10, 2021 at 09:50 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `myagness_latest`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_setting`
--

CREATE TABLE `admin_setting` (
  `id` int(11) NOT NULL,
  `phone_no` varchar(20) NOT NULL DEFAULT '+91 21456987',
  `email` varchar(255) NOT NULL DEFAULT 'support@email.com',
  `address` text,
  `pp` text NOT NULL,
  `notification` int(11) NOT NULL DEFAULT '1',
  `currency_symbol` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '$',
  `currency` varchar(255) NOT NULL DEFAULT 'USD',
  `time_slot_length` int(11) NOT NULL DEFAULT '30',
  `verification` int(11) NOT NULL DEFAULT '0',
  `sms_gateway` varchar(20) NOT NULL DEFAULT 'twilio',
  `country_code` varchar(4) NOT NULL DEFAULT '+91',
  `offline_payment` int(11) NOT NULL DEFAULT '1',
  `stipe_status` int(11) NOT NULL DEFAULT '0',
  `paypal_status` int(11) NOT NULL DEFAULT '0',
  `razor_status` int(11) NOT NULL DEFAULT '0',
  `ios_version` varchar(255) NOT NULL DEFAULT '1.0.2',
  `android_version` varchar(255) NOT NULL DEFAULT '1.0.5',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `otp` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_setting`
--

INSERT INTO `admin_setting` (`id`, `phone_no`, `email`, `address`, `pp`, `notification`, `currency_symbol`, `currency`, `time_slot_length`, `verification`, `sms_gateway`, `country_code`, `offline_payment`, `stipe_status`, `paypal_status`, `razor_status`, `ios_version`, `android_version`, `created_at`, `updated_at`, `otp`) VALUES
(1, '+4478788778772', 'support@email.com', 'Massachusetts Turnpike', '<h2 style=\"letter-spacing: normal; padding: 0px; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 45px; line-height: 48px; margin: 24px 0px; color: rgb(97, 97, 97);\">Privacy Policy d</h2><p style=\"letter-spacing: normal; padding: 0px; line-height: 24px; font-size: 14px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif;\">[Developer/Company name] built the [App Name] app as [open source/free/freemium/ad-supported/commercial] app. This SERVICE is provided by [Developer/Company name] [at no cost] and is intended for use as is.</p><p style=\"letter-spacing: normal; padding: 0px; line-height: 24px; font-size: 14px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif;\">This page is used to inform visitors regarding [my/our] policies with the collection, use, and disclosure of Personal Information if anyone decided to use [my/our] Service.</p><p style=\"letter-spacing: normal; padding: 0px; line-height: 24px; font-size: 14px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif;\">If you choose to use [my/our] Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that [I/We] collect is used for providing and improving the Service. [I/We] will not use or share your information with anyone except as described in this Privacy Policy.</p><p style=\"letter-spacing: normal; padding: 0px; line-height: 24px; font-size: 14px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif;\">The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at [App Name] unless otherwise defined in this Privacy Policy.</p><div><strong style=\"letter-spacing: normal; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif; font-size: 14px;\">Information Collection and Use</strong></div><div><p style=\"letter-spacing: normal; padding: 0px; line-height: 24px; font-size: 14px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif;\">For a better experience, while using our Service, [I/We] may require you to provide us with certain personally identifiable information[add whatever else you collect here, e.g. users name, address, location, pictures] The information that [I/We] request will be [retained on your device and is not collected by [me/us] in any way]/[retained by us and used as described in this privacy policy].</p><p style=\"letter-spacing: normal; padding: 0px; line-height: 24px; font-size: 14px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif;\">The app does use third party services that may collect information used to identify you.</p><div style=\"letter-spacing: normal; color: rgb(97, 97, 97); font-family: Roboto, Helvetica, sans-serif; font-size: 14px;\"><p style=\"padding: 0px; line-height: 24px; letter-spacing: 0px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px;\">Link to privacy policy of third party service providers used by the app</p><ul style=\"letter-spacing: 0px; line-height: 24px;\"><li><a href=\"https://www.google.com/policies/privacy/\" target=\"_blank\" style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); color: rgb(68, 138, 255);\">Google Play Services</a></li></ul><p style=\"padding: 0px; line-height: 24px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px;\"><strong>Log Data</strong></p><p style=\"padding: 0px; line-height: 24px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px;\">[I/We] want to inform you that whenever you use [my/our] Service, in a case of an error in the app [I/We] collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing [my/our] Service, the time and date of your use of the Service, and other statistics.</p></div><br></div>', 1, '₹', 'usd', 60, 0, 'twilio', '+913', 1, 1, 1, 1, '2.2.03', '20.2.2', NULL, '2021-02-20 02:25:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'default.png',
  `address` text,
  `device_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `liked_salon` text,
  `otp` varchar(6) DEFAULT '',
  `noti` int(11) NOT NULL DEFAULT '1',
  `verified` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `app_role` int(11) NOT NULL DEFAULT '1',
  `address_added` int(11) NOT NULL DEFAULT '0',
  `longitude` decimal(13,10) DEFAULT NULL,
  `latitude` decimal(13,10) DEFAULT NULL,
  `pin_code` int(11) DEFAULT NULL,
  `unique_identification_code` varchar(500) DEFAULT NULL,
  `token` text,
  `admin_approval` int(11) NOT NULL DEFAULT '1',
  `provider_id` int(11) DEFAULT NULL,
  `provider` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `name`, `email`, `password`, `phone_no`, `image`, `address`, `device_token`, `status`, `liked_salon`, `otp`, `noti`, `verified`, `created_at`, `updated_at`, `app_role`, `address_added`, `longitude`, `latitude`, `pin_code`, `unique_identification_code`, `token`, `admin_approval`, `provider_id`, `provider`) VALUES
(9, 'der', 'user@user.com', '$2y$10$tqKuEqlmW1p5FjXG8d03N.wLfJL5hEL2T7M8PDEUOIb3hUWuQ.VxK', '123456', 'default.png', NULL, NULL, 0, NULL, '', 1, 1, '2020-05-23 05:14:43', '2021-01-30 19:22:08', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(10, 'der', 'user@user.com1', '$2y$10$jlSTTzjOhsqOpw8/Z03IxOwH1gNde8dMR9ufNqB7r8wYs1G0ypwqi', '123456sad', 'default.png', NULL, NULL, 1, NULL, '', 1, 1, '2020-05-23 09:10:10', '2020-05-23 09:10:10', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(11, 'test', 'test@gmail.com', '$2y$10$tqKuEqlmW1p5FjXG8d03N.wLfJL5hEL2T7M8PDEUOIb3hUWuQ.VxK', '12345689', '5ed5f91ceae46.jpg', 'House No 123 Block J Near Hafeez center, Lahore', NULL, 1, NULL, '8629', 1, 1, '2020-05-25 06:21:59', '2021-02-14 21:18:25', 1, 1, '74.2533000000', '31.4728000000', NULL, NULL, NULL, 1, NULL, NULL),
(12, 'rio', 'rio@gmail.com', '$2y$10$ub9V.eJZJVa1PRQKltjdFuk2dYEi8r2ZXt1XiOJ6h5KIlRmtZ7D0C', '8200284277', 'default.png', NULL, NULL, 1, NULL, '9021', 1, 1, '2020-05-25 09:19:26', '2020-05-25 09:20:10', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(13, 'Aqib', 'aqib.yaqoob@azure-i.com', '$2y$10$nMXKOYwKHTLCJV5b.uqDEOJjOx00sDRNsP157p45xDIq5ZKPNfvoq', '+923014725939', 'default.png', NULL, NULL, 1, NULL, '', 1, 1, '2021-02-06 15:21:00', '2021-02-06 15:21:00', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(14, 'Aqib Formanite', 'formanite.yaqoob92@gmail.com', '$2y$10$ciUq.6EECDaU0XCgcQVyau47GSTu.FmG2C2.zHCuZvjDmfEBasVfO', '+923014725666', 'default.png', NULL, 'asdasas8763874637846', 1, NULL, '', 1, 1, '2021-02-06 16:40:52', '2021-03-31 02:57:38', 1, 0, NULL, NULL, NULL, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMTJhZmQ2MzhhMTZhYTkxOWEwODZiZjhiYmFmZDYwYTg0Zjg3OTIyYTJjZmYxMzlhNWJhMTA2NjU3YjZmZDY1MzczYjdiNGM5OTU1NDdjMWMiLCJpYXQiOjE2MTYwMDQ5MTIsIm5iZiI6MTYxNjAwNDkxMiwiZXhwIjoxNjQ3NTQwOTEyLCJzdWIiOiIxNCIsInNjb3BlcyI6W119.DfsHNTAqcFuO8CzIMMCpcYl5MOzl6wFFS-j9JeyVegUb0ldf1-z4H-qBBQgUd0Vkh1ILBIeyfhGLhglkvPA31dRdRq6EBygAAsYDZ6Onog9dHCA5kBNSwJAlFb-ItgoZY9DgEt2FOIm5W4OPlDXwMT0sBoxquOGQD4ZkqgWXjbKBWPaChbz_LAY7CgV90rg4C121dYLdQBZAAB4cFHNH3HJBuVv5J8HN-ke3CrVDqeqNUethe4UzbVvaP6l-hj62zugC1Fjs5uygcbsuslRkE9lVOMDpK7NOvEqEBpG10udthhUj1_Q3EqPJus4RbNwQX1z9vfanux6EWfvW2un-48w9wMaRYLZvOR333TQ7RNpPlo13o1VE8E2FzJIi1GTlmxZ3KijEjTQvDnw5scyz9gaamtEaSPcjBfw6LIimmRfHoI9i4I4GLTvTT_uSiYGUMpcRaoU37adjnyolYNbBYMb8wJ5VDT7SbcvcDKXq-25ylbbAT5fidW1N3bQuDAj8WxMm7gersP-uLnAVwXKD5v7cMLcuUZ0fhrkNts9xO0nUnGZamJi-FDiuhLKIK4aDAP8BnKwH8wOuj34CFwnNFLTQ0f3ZSZnYTQaH5nZmxjdDwEjQMqHf4ylRzD1X-y1hOtcICT12ipyKtbqv2vYRoe_o0yYL6XqdHqtvDSEBISk', 1, NULL, NULL),
(15, 'usama', '`usamaahm61@gmail.com', '$2y$10$zc897axqVbGpwrL1usbFReSeOrYP3TzPVqa1EmNTtWsnosQHAxlbK', '93224577544', 'default.png', NULL, NULL, 1, NULL, '6283', 1, 0, '2021-02-06 19:11:52', '2021-02-06 19:11:52', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(16, 'Usama', 'usamaahm61@gmail.com', '$2y$10$tqKuEqlmW1p5FjXG8d03N.wLfJL5hEL2T7M8PDEUOIb3hUWuQ.VxK', '03224577544', 'default.png', '124 D3 Wapda Town Lahore', 'abced', 1, NULL, '', 1, 1, '2021-02-06 19:18:33', '2021-03-06 23:48:59', 1, 1, '74.2533000000', '31.4728000000', 2266, '40f0bcd0-9989-4b68-8099-ca150bc03adb', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZGUwNDhmOGZjZTU5MDdjNTM0Y2VkMDVhZDFjYjEyYzkwZmUwMjNlZDA5OTRiYzMxMGQwMjIwMTBlMjIwODZkMzA5ODMxMDY4MzU5YjdlODIiLCJpYXQiOjE2MTUwNTQ3MzksIm5iZiI6MTYxNTA1NDczOSwiZXhwIjoxNjQ2NTkwNzM5LCJzdWIiOiIxNiIsInNjb3BlcyI6W119.gvZ_3fVGRect5pfo_kXvNWgpg1rVajth6Pm2osbifYUeHAmBASf-_6AKdfeQfTg3gFGMyn1obYlrpVza0auteHKXyU9T6scF104JsunWaszJJJjylU8mlxFjlX6jtbttRFwd7Y94YpYgQI1kKV3yZ_JSPDYkrxEFPSOrfR1QTjeIxfOQEsoqncXjcwDkKkOXkv2L2lxmSgB6W-3weWWa1qRGJlrp9-Bic3grUZHGJZ6lBezBDiba9_zTywmjpsYppU9jiyUIfV3OcSLmeG3j1fC_bxRsmDMbTOvI9sbV_kiJsjzuM6r0IqYDK6JUP8cMbiJd4sKUctvgYe4jTB1t7z4QJM57RMm9zHRY-vnLFcSJTx6eLnNtOVwvej_D3UWvASY1BZjTUM-rb-q1WB95xHmH4iSnBs1iC99gtoa_d5beuCOOx6D9qp8QW4pkjLBltZ3nnJlvB6U_i8RoE1loOvLPK1mlZqov5ZLo9UEUluk6KPf2-JiezmyCL-ENf-aOYLPlwysp69lWOOH70OoS0Kwxcf9bEQ8XyV0f-2NakeETLyuPRWqZ5GKziWvvWLEajmDxkn3T5CbHGN7x1XuA3Lba7F7Jx-GIU3LlGoTctxdVlrnyjO8SR9lGwD5fJiYnE0MFUjPffMCWCyvs0r6wyLOyNGZ4sQ3ta6m2RNnrpl0', 1, NULL, NULL),
(17, 'usama', 'usamaahm61+1@gmail.com', '$2y$10$JRnPJo1bYLK6EbWLyCxwyezb9e2ar8OQICtggbSwFvgjeRQHFxrci', '03224433443', 'default.png', NULL, NULL, 1, NULL, '', 1, 1, '2021-02-14 17:23:35', '2021-02-14 17:24:38', 2, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(18, 'khizar', 'khizartouqeer@gmail.com', '$2y$10$rLdqM3Wt/.j55sLIv3jDn.i9WiapN0CVZ6aBv3GJ5VZ0AqVbvmhjO', '03338686001', 'default.png', NULL, NULL, 1, NULL, '0813', 1, 0, '2021-02-16 13:47:00', '2021-02-16 13:47:00', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(19, 'khizar', 'khizartauqeer@gmail.com', '$2y$10$kziBV0cP2C8XmyvgB0VoPuyl.PzvrmYFiVtALqyE01joyogrmANRq', '03338720828', 'default.png', NULL, NULL, 1, NULL, '8129', 1, 0, '2021-02-16 22:49:46', '2021-02-16 22:49:46', 2, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(20, 'khizar', 'khizartauqeer@gmail.com', '$2y$10$7OVGo3FvCtDhUMh8XgQCpOKJwzBqu1HIg5C1qD/fhLXIIExWPWFxG', '03338720828', 'default.png', NULL, NULL, 1, NULL, '7450', 1, 0, '2021-02-16 22:49:46', '2021-02-16 22:49:46', 2, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(21, 'Usama Ravian', 'usamaahm61+2@gmail.com', '$2y$10$tqKuEqlmW1p5FjXG8d03N.wLfJL5hEL2T7M8PDEUOIb3hUWuQ.VxK', '+923224577544', 'default.png', NULL, NULL, 1, NULL, '', 1, 1, '2021-02-24 00:10:27', '2021-02-24 00:11:06', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(22, 'Aqib Formanite 1', 'formanite.yaqoob92+1@gmail.com', '$2y$10$GL6zep4kBKj80fqx.2Ch6eUgKd75ZkvmO9piMK1PXPykqqKdvcFMe', '+923014725699', 'default.png', 'House No 123 Block J Near Hafeez center, Lahore', NULL, 1, NULL, '', 1, 1, '2021-02-27 14:03:00', '2021-02-27 14:05:26', 1, 1, '74.2533000000', '31.4728000000', NULL, NULL, NULL, 1, NULL, NULL),
(23, 'Aqib Formanite 2', 'formanite.yaqoob92+2@gmail.com', '$2y$10$ObN9D2r.NT9ZrBSwjEzjtOsyXBz.Sy/lQwJYjg9wyXT9VtNBj1pDa', '+923014725690', 'default.png', NULL, NULL, 1, NULL, '6198', 1, 0, '2021-02-27 18:42:01', '2021-02-27 18:42:01', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(24, 'Aqib Formanite 3', 'formanite.yaqoob92+3@gmail.com', '$2y$10$G.kfAtfqK9JTVCLr6pXX4OKSiC3fpLJUUO8Fv9lWPgi0T/VCDyAk2', '+92301472566645', 'default.png', NULL, NULL, 1, NULL, '4071', 1, 0, '2021-02-27 18:42:58', '2021-02-27 18:42:58', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(25, 'Aqib Formanite 5', 'formanite.yaqoob92+5@gmail.com', '$2y$10$OlZjKbzFlwErTJD3IHT8LOZn.4gp979e.QhmR9frIQjZ3Shnk/ms6', '+9230147256876', 'default.png', NULL, NULL, 1, NULL, '7651', 1, 0, '2021-02-27 18:45:01', '2021-02-27 18:45:01', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(26, 'Aqib Formanite 9', 'formanite.yaqoob92+9@gmail.com', '$2y$10$SVV/mQwhlWtfYOJBisAvfOcvIV0dp2rwehxsFWFHiRCz1KBMrFsYm', '+92301411111', 'default.png', NULL, NULL, 1, NULL, '8139', 1, 0, '2021-02-27 19:10:22', '2021-02-27 19:10:22', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(27, 'Emp User 1', 'employeeuser_1@gmail.com', '$2y$10$SDBOXTzau9Cz9o2kDMwy0uxuqJHnUEdNZ1TcpRHzQprZbxQrXFyuy', '+92301663773', 'default.png', 'HOuse 123 Branch Nill', NULL, 1, NULL, '1263', 1, 0, '2021-03-01 13:27:48', '2021-03-01 13:27:48', 1, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(28, 'Emp User 2', 'employeeuser_2@gmail.com', '$2y$10$ZlJyEr9gkL9MFFXUX9oI5./InUNNntkm6tgck1j5hogrAgsq2ajky', '+9230166377354', 'default.png', 'HOuse 1233 Branch Nill', 'abced', 1, NULL, '9502', 1, 1, '2021-03-01 13:28:50', '2021-03-17 00:33:35', 2, 0, NULL, NULL, NULL, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZGJhNWQ0NWRkMDI3YjljN2Q3MmEyMWJjYmU4YTE2MDQxMjgyODQ3OTY3YTYxNDJjZWE2ODA1Njk2YWZmYmU4ZjI4ZDgxYTg5MzhhNWY0ZmEiLCJpYXQiOjE2MTU5MjE0MTUsIm5iZiI6MTYxNTkyMTQxNSwiZXhwIjoxNjQ3NDU3NDE1LCJzdWIiOiIyOCIsInNjb3BlcyI6W119.efLIL0RZ7WRFLCA6zO5m7c1MfljCcr-X5hXvpmm1hu4iPsOn9bI5gaFbWKGzV0BKJNQo02WaFENwN4i3uG_e3BVWC35qO0zglsiBBCDOWo_Iwl0zBl6qG-6xTtuhFZ86BGCQs11yrFQku5IUUHPP3CYit1hNxgFWEFuoEhW-e8wX1G7xIBnMQgkMdXutq3UsbKGjdWbq4vXkNPYmXgrSZd5E7N6i4-lkxFQbV-mQ1Kq7-ewwRzscVe9FvhNjRg_o-6PaDu4E_nPgMCpONaD2WL1PSjZ3vCHzQT-4ruXTfYmiwwMouaJ0qvsIauxDae-eGTqbjzG-pQBv96FB0-uZiZCXvAF63doxNJ-rIt-7Fj_zrvvJooRJpgJYZdl64tixi8JxHWCaFKcfWYlP9pDrIrK3kYnbwwBs7uM_4n7Arld4DmQXUvjd67IRr-33DYIWVsTMRlhXXYauqZ19dmph_6jtAEI3iNGRn-e6VokiM54FHxKV8cIIPltfkD8WNrPzi4Ll-LfnSMRd-x_H3Qn-cKPwHd6V4POnpzjR7eFVc0MW35i6_6vlTzoV3SS3xKuo0pUxv249_UCaFh9jyo9LRhKnnw-EplyOGDCv0IucBDXaicqMO3r_cBhZZGRPATmQgkhfZkG24HGVMxEsetT_DQSF9zpzg6bKiNO7aQ_OMrE', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_info`
--

CREATE TABLE `auth_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auth_info`
--

INSERT INTO `auth_info` (`id`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, '2021-04-22 08:54:55', '2021-04-22 08:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `booking_child`
--

CREATE TABLE `booking_child` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `user_email` varchar(191) DEFAULT NULL,
  `emp_email` varchar(191) DEFAULT NULL,
  `branch_name` varchar(191) DEFAULT NULL,
  `total` decimal(9,3) NOT NULL DEFAULT '0.000',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_child`
--

INSERT INTO `booking_child` (`id`, `booking_id`, `service_id`, `emp_id`, `duration`, `start_time`, `end_time`, `created_at`, `updated_at`, `branch_id`, `user_id`, `user_email`, `emp_email`, `branch_name`, `total`, `status`) VALUES
(1, 1, 1, 1, 30, '2020-06-03 09:00:00', '2020-06-03 09:30:00', '2020-06-03 17:55:36', '2020-06-03 17:55:36', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(2, 2, 9, 2, 20, '2020-06-04 09:00:00', '2020-06-04 09:20:00', '2020-06-04 11:42:14', '2020-06-04 11:42:14', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(3, 3, 1, 1, 30, '2020-06-05 20:00:00', '2020-06-05 20:30:00', '2020-06-04 13:08:25', '2020-06-04 13:08:25', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(4, 3, 2, 1, 40, '2020-06-05 20:30:00', '2020-06-05 21:10:00', '2020-06-04 13:08:25', '2020-06-04 13:08:25', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(5, 3, 3, 1, 50, '2020-06-05 21:10:00', '2020-06-05 22:00:00', '2020-06-04 13:08:25', '2020-06-04 13:08:25', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(6, 3, 7, 2, 20, '2020-06-05 22:00:00', '2020-06-05 22:20:00', '2020-06-04 13:08:25', '2020-06-04 13:08:25', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(7, 3, 8, 2, 45, '2020-06-05 22:20:00', '2020-06-05 23:05:00', '2020-06-04 13:08:25', '2020-06-04 13:08:25', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(8, 3, 9, 2, 20, '2020-06-05 23:05:00', '2020-06-05 23:25:00', '2020-06-04 13:08:25', '2020-06-04 13:08:25', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(9, 4, 1, 1, 30, '2020-06-05 13:00:00', '2020-06-05 13:30:00', '2020-06-04 14:36:50', '2021-05-28 00:55:44', 1, NULL, NULL, NULL, NULL, '0.000', 4),
(10, 4, 2, 1, 40, '2020-06-05 13:30:00', '2020-06-05 14:10:00', '2020-06-04 14:36:50', '2021-05-28 00:55:44', 1, NULL, NULL, NULL, NULL, '0.000', 4),
(11, 5, 2, 1, 40, '2020-06-04 20:00:00', '2020-06-04 20:40:00', '2020-06-04 14:53:03', '2021-04-14 20:01:57', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(12, 6, 1, 1, 30, '2020-06-04 17:00:00', '2020-06-04 17:30:00', '2020-06-04 15:14:06', '2020-06-04 15:14:06', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(13, 7, 2, 1, 40, '2020-06-09 16:00:00', '2020-06-09 16:40:00', '2020-06-04 16:15:15', '2020-06-04 16:15:15', 1, NULL, NULL, NULL, NULL, '0.000', 0),
(14, 16, 2, 28, 0, '2021-02-25 09:00:00', '2021-02-25 09:00:00', '2021-02-24 00:00:00', '2021-03-07 20:51:30', 1, 16, NULL, NULL, NULL, '0.000', 1),
(15, 19, 7, 28, 0, '2021-03-06 14:00:00', '2021-03-06 00:00:00', '2021-02-26 00:00:00', '2021-02-26 00:00:00', 1, 16, NULL, NULL, NULL, '0.000', 1),
(16, 19, 2, 28, 0, '2021-03-06 14:00:00', '2021-03-06 14:00:00', '2021-02-26 00:00:00', '2021-02-26 00:00:00', 1, 16, NULL, NULL, NULL, '35.000', 1),
(17, 19, 1, 28, 0, '2021-03-06 14:00:00', '2021-03-06 14:00:00', '2021-02-26 00:00:00', '2021-02-26 00:00:00', 1, 16, 'usamaahm61@gmail.com', 'employeeuser_2@gmail.com', '1', '20.000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_master`
--

CREATE TABLE `booking_master` (
  `id` int(11) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `total` float NOT NULL,
  `discount` float NOT NULL DEFAULT '0',
  `duration` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 = waiting 1 =confirm 2=complate 3=cancel',
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0= no 1 = yes',
  `payment_token` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) NOT NULL DEFAULT 'Offline',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `rejected_at` datetime DEFAULT NULL,
  `cancel_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_master`
--

INSERT INTO `booking_master` (`id`, `booking_id`, `user_id`, `branch_id`, `start_time`, `end_time`, `offer_id`, `total`, `discount`, `duration`, `status`, `payment_status`, `payment_token`, `payment_method`, `updated_at`, `created_at`, `rejected_at`, `cancel_at`, `completed_at`) VALUES
(1, 'bhk-5ed796', 11, 1, '2020-06-03 09:00:00', '2020-06-03 09:30:00', NULL, 30, 0, 30, 2, 0, NULL, 'Offline', '2020-06-04 11:39:06', '2020-06-03 17:55:36', NULL, NULL, NULL),
(2, 'bhk-5ed890', 11, 1, '2020-06-04 09:00:00', '2020-06-04 09:20:00', NULL, 35, 0, 20, 3, 1, NULL, 'Offline', '2020-06-04 13:12:32', '2020-06-04 11:42:14', NULL, NULL, NULL),
(3, 'bhk-5ed8a4', 11, 1, '2020-06-05 20:00:00', '2020-06-05 23:25:00', NULL, 210, 0, 205, 2, 1, NULL, 'Offline', '2020-06-04 13:10:29', '2020-06-04 13:08:25', NULL, NULL, NULL),
(4, 'bhk-5ed8b9', 11, 3, '2020-06-05 13:00:00', '2020-06-05 14:10:00', NULL, 55, 0, 70, 5, 1, NULL, 'Offline', '2021-05-28 00:55:44', '2020-06-04 14:36:50', NULL, NULL, NULL),
(5, 'bhk-5ed8bd', 11, 1, '2020-06-04 20:00:00', '2020-06-04 20:40:00', NULL, 25, 0, 40, 5, 0, NULL, 'Offline', '2021-05-28 00:38:21', '2020-06-04 14:53:03', NULL, NULL, NULL),
(6, 'bhk-5ed8c2', 11, 1, '2020-06-04 17:00:00', '2020-06-04 17:30:00', NULL, 30, 0, 30, 3, 0, NULL, 'Offline', '2021-04-11 16:43:42', '2020-06-04 15:14:06', NULL, NULL, NULL),
(7, 'bhk-5ed8d0', 11, 1, '2020-06-09 16:00:00', '2020-06-09 16:40:00', NULL, 25, 0, 40, 2, 1, 'tok_1GqGZSFluOgnsqwfZN30BsWg', 'Strip', '2021-02-27 16:23:38', '2020-06-04 16:15:15', NULL, NULL, NULL),
(16, 'bhk-603684', 16, NULL, '2021-02-25 09:00:00', '2021-02-25 09:00:00', NULL, 25, 0, 0, 2, 0, NULL, 'Offline', '2021-04-11 16:44:01', '2021-02-24 22:23:37', NULL, NULL, NULL),
(19, 'bhk-603809', 16, NULL, '2021-03-06 06:00:00', '2021-03-06 06:00:00', NULL, 90, 0, 0, 2, 0, NULL, 'Offline', '2021-04-01 23:50:42', '2021-02-26 02:04:51', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `for_who` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `icon` varchar(50) NOT NULL DEFAULT 'default.png',
  `start_time` time NOT NULL DEFAULT '09:00:00',
  `end_time` time NOT NULL DEFAULT '23:00:00',
  `category` text,
  `manager` text,
  `employee` text,
  `is_featured` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `longitude` decimal(13,10) DEFAULT NULL,
  `latitude` decimal(13,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name`, `address`, `for_who`, `description`, `icon`, `start_time`, `end_time`, `category`, `manager`, `employee`, `is_featured`, `status`, `created_at`, `updated_at`, `deleted_at`, `longitude`, `latitude`) VALUES
(1, 'Addictive Beauty', 'West minister Business Road, UK', 0, 'At Addictive Beauty Salon We never compromise on quality to  the highest standards of Hygiene and products,', '5ed795bb3710c.png', '09:00:00', '20:00:00', '3,1', '12', '8,9', 1, 1, '2020-06-03 17:47:56', '2020-06-03 17:58:12', NULL, '74.2533000000', '31.4728000000'),
(2, 'Barbarella Salon', 'rajkot  gujrat', 2, 'At Addictive Beauty Salon We never compromise on quality to insur', '5ed79560a64a0.png', '09:00:00', '19:00:00', '1', '11', '9', 1, 1, '2020-06-03 17:49:44', '2020-06-04 12:59:39', NULL, '74.3505010000', '31.5315518000'),
(3, 'The Makeover Place', '341 Saint Nicholas Ave, New York, NY 10027, USA', 1, 'At Addictive Beauty Salon We never compromise on qual', '5ed795abc27b7.png', '08:00:00', '18:00:00', '3,1,2', '13', '10,8,9', 0, 1, '2020-06-03 17:50:59', '2020-06-03 17:58:31', NULL, '74.3480830000', '31.5619200000'),
(4, 'Testing', 'jhaskjdhajks', 0, 'hgdfhjsgdfhjf', '605f61c70afba.png', '23:47:00', '12:47:00', '3', '1', '17,16,10,8,9', 1, 1, '2021-03-27 22:18:07', '2021-03-27 22:18:07', NULL, NULL, '343.2343433000');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `total` decimal(9,3) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `delivery_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_items` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `email`, `user_id`, `total`, `discount`, `delivery_address`, `total_items`, `created_at`, `updated_at`) VALUES
(4, 'formanite.yaqoob92+1@gmail.com', 22, '35.000', 0, NULL, 1, '2021-02-27 09:44:30', '2021-02-27 09:45:50');

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `amount` decimal(9,3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `checkout` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `email`, `user_id`, `service_id`, `cart_id`, `cat_id`, `amount`, `created_at`, `updated_at`, `branch_id`, `checkout`) VALUES
(50, 'formanite.yaqoob92+1@gmail.com', 22, 7, 4, 3, '35.000', '2021-02-27 09:44:34', '2021-02-27 09:44:34', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT 'default.png',
  `is_trending` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `icon`, `is_trending`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hair', '5ed78dd951db3.png', 1, 1, '2020-06-03 17:17:37', '2021-03-18 23:28:13', '2021-03-18 23:28:13'),
(2, 'Spa', '5ed78df24b612.png', 1, 1, '2020-06-03 17:18:02', '2020-06-03 17:18:02', NULL),
(3, 'Eyebrows & Lashes', '5ed78e062fd1a.png', 0, 1, '2020-06-03 17:18:22', '2020-06-03 17:18:22', NULL),
(4, 'Nail Polish', '605f56008a2e3.png', 0, 1, '2020-06-03 17:18:43', '2021-03-27 21:27:52', NULL),
(5, 'Nail Paiting', '5ed78e373e025.png', 0, 1, '2020-06-03 17:19:11', '2021-03-19 00:01:24', '2021-03-19 00:01:24');

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `total` decimal(9,3) NOT NULL,
  `cart_ids` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `country` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `country`, `currency`, `code`, `symbol`) VALUES
(1, 'Albania', 'Leke', 'ALL', 'Lek'),
(2, 'America', 'Dollars', 'USD', '$'),
(3, 'Afghanistan', 'Afghanis', 'AFN', '؋'),
(4, 'Argentina', 'Pesos', 'ARS', '$'),
(5, 'Aruba', 'Guilders', 'AWG', 'Afl'),
(6, 'Australia', 'Dollars', 'AUD', '$'),
(7, 'Azerbaijan', 'New Manats', 'AZN', '₼'),
(8, 'Bahamas', 'Dollars', 'BSD', '$'),
(9, 'Barbados', 'Dollars', 'BBD', '$'),
(10, 'Belarus', 'Rubles', 'BYR', 'p.'),
(11, 'Belgium', 'Euro', 'EUR', '€'),
(12, 'Beliz', 'Dollars', 'BZD', 'BZ$'),
(13, 'Bermuda', 'Dollars', 'BMD', '$'),
(14, 'Bolivia', 'Bolivianos', 'BOB', '$b'),
(15, 'Bosnia and Herzegovina', 'Convertible Marka', 'BAM', 'KM'),
(16, 'Botswana', 'Pula', 'BWP', 'P'),
(17, 'Bulgaria', 'Leva', 'BGN', 'Лв.'),
(18, 'Brazil', 'Reais', 'BRL', 'R$'),
(19, 'Britain (United Kingdom)', 'Pounds', 'GBP', '£\r\n'),
(20, 'Brunei Darussalam', 'Dollars', 'BND', '$'),
(21, 'Cambodia', 'Riels', 'KHR', '៛'),
(22, 'Canada', 'Dollars', 'CAD', '$'),
(23, 'Cayman Islands', 'Dollars', 'KYD', '$'),
(24, 'Chile', 'Pesos', 'CLP', '$'),
(25, 'China', 'Yuan Renminbi', 'CNY', '¥'),
(26, 'Colombia', 'Pesos', 'COP', '$'),
(27, 'Costa Rica', 'Colón', 'CRC', '₡'),
(28, 'Croatia', 'Kuna', 'HRK', 'kn'),
(29, 'Cuba', 'Pesos', 'CUP', '₱'),
(30, 'Cyprus', 'Euro', 'EUR', '€'),
(31, 'Czech Republic', 'Koruny', 'CZK', 'Kč'),
(32, 'Denmark', 'Kroner', 'DKK', 'kr'),
(33, 'Dominican Republic', 'Pesos', 'DOP ', 'RD$'),
(34, 'East Caribbean', 'Dollars', 'XCD', '$'),
(35, 'Egypt', 'Pounds', 'EGP', '£'),
(36, 'El Salvador', 'Colones', 'SVC', '$'),
(37, 'England (United Kingdom)', 'Pounds', 'GBP', '£'),
(38, 'Euro', 'Euro', 'EUR', '€'),
(39, 'Falkland Islands', 'Pounds', 'FKP', '£'),
(40, 'Fiji', 'Dollars', 'FJD', '$'),
(41, 'France', 'Euro', 'EUR', '€'),
(42, 'Ghana', 'Cedis', 'GHC', 'GH₵'),
(43, 'Gibraltar', 'Pounds', 'GIP', '£'),
(44, 'Greece', 'Euro', 'EUR', '€'),
(45, 'Guatemala', 'Quetzales', 'GTQ', 'Q'),
(46, 'Guernsey', 'Pounds', 'GGP', '£'),
(47, 'Guyana', 'Dollars', 'GYD', '$'),
(48, 'Holland (Netherlands)', 'Euro', 'EUR', '€'),
(49, 'Honduras', 'Lempiras', 'HNL', 'L'),
(50, 'Hong Kong', 'Dollars', 'HKD', '$'),
(51, 'Hungary', 'Forint', 'HUF', 'Ft'),
(52, 'Iceland', 'Kronur', 'ISK', 'kr'),
(53, 'India', 'Rupees', 'INR', '₹'),
(54, 'Indonesia', 'Rupiahs', 'IDR', 'Rp'),
(55, 'Iran', 'Rials', 'IRR', '﷼'),
(56, 'Ireland', 'Euro', 'EUR', '€'),
(57, 'Isle of Man', 'Pounds', 'IMP', '£'),
(58, 'Israel', 'New Shekels', 'ILS', '₪'),
(59, 'Italy', 'Euro', 'EUR', '€'),
(60, 'Jamaica', 'Dollars', 'JMD', 'J$'),
(61, 'Japan', 'Yen', 'JPY', '¥'),
(62, 'Jersey', 'Pounds', 'JEP', '£'),
(63, 'Kazakhstan', 'Tenge', 'KZT', '₸'),
(64, 'Korea (North)', 'Won', 'KPW', '₩'),
(65, 'Korea (South)', 'Won', 'KRW', '₩'),
(66, 'Kyrgyzstan', 'Soms', 'KGS', 'Лв'),
(67, 'Laos', 'Kips', 'LAK', '	₭'),
(68, 'Latvia', 'Lati', 'LVL', 'Ls'),
(69, 'Lebanon', 'Pounds', 'LBP', '£'),
(70, 'Liberia', 'Dollars', 'LRD', '$'),
(71, 'Liechtenstein', 'Switzerland Francs', 'CHF', 'CHF'),
(72, 'Lithuania', 'Litai', 'LTL', 'Lt'),
(73, 'Luxembourg', 'Euro', 'EUR', '€'),
(74, 'Macedonia', 'Denars', 'MKD', 'Ден\r\n'),
(75, 'Malaysia', 'Ringgits', 'MYR', 'RM'),
(76, 'Malta', 'Euro', 'EUR', '€'),
(77, 'Mauritius', 'Rupees', 'MUR', '₹'),
(78, 'Mexico', 'Pesos', 'MXN', '$'),
(79, 'Mongolia', 'Tugriks', 'MNT', '₮'),
(80, 'Mozambique', 'Meticais', 'MZN', 'MT'),
(81, 'Namibia', 'Dollars', 'NAD', '$'),
(82, 'Nepal', 'Rupees', 'NPR', '₹'),
(83, 'Netherlands Antilles', 'Guilders', 'ANG', 'ƒ'),
(84, 'Netherlands', 'Euro', 'EUR', '€'),
(85, 'New Zealand', 'Dollars', 'NZD', '$'),
(86, 'Nicaragua', 'Cordobas', 'NIO', 'C$'),
(87, 'Nigeria', 'Nairas', 'NGN', '₦'),
(88, 'North Korea', 'Won', 'KPW', '₩'),
(89, 'Norway', 'Krone', 'NOK', 'kr'),
(90, 'Oman', 'Rials', 'OMR', '﷼'),
(91, 'Pakistan', 'Rupees', 'PKR', '₹'),
(92, 'Panama', 'Balboa', 'PAB', 'B/.'),
(93, 'Paraguay', 'Guarani', 'PYG', 'Gs'),
(94, 'Peru', 'Nuevos Soles', 'PEN', 'S/.'),
(95, 'Philippines', 'Pesos', 'PHP', 'Php'),
(96, 'Poland', 'Zlotych', 'PLN', 'zł'),
(97, 'Qatar', 'Rials', 'QAR', '﷼'),
(98, 'Romania', 'New Lei', 'RON', 'lei'),
(99, 'Russia', 'Rubles', 'RUB', '₽'),
(100, 'Saint Helena', 'Pounds', 'SHP', '£'),
(101, 'Saudi Arabia', 'Riyals', 'SAR', '﷼'),
(102, 'Serbia', 'Dinars', 'RSD', 'ع.د'),
(103, 'Seychelles', 'Rupees', 'SCR', '₹'),
(104, 'Singapore', 'Dollars', 'SGD', '$'),
(105, 'Slovenia', 'Euro', 'EUR', '€'),
(106, 'Solomon Islands', 'Dollars', 'SBD', '$'),
(107, 'Somalia', 'Shillings', 'SOS', 'S'),
(108, 'South Africa', 'Rand', 'ZAR', 'R'),
(109, 'South Korea', 'Won', 'KRW', '₩'),
(110, 'Spain', 'Euro', 'EUR', '€'),
(111, 'Sri Lanka', 'Rupees', 'LKR', '₹'),
(112, 'Sweden', 'Kronor', 'SEK', 'kr'),
(113, 'Switzerland', 'Francs', 'CHF', 'CHF'),
(114, 'Suriname', 'Dollars', 'SRD', '$'),
(115, 'Syria', 'Pounds', 'SYP', '£'),
(116, 'Taiwan', 'New Dollars', 'TWD', 'NT$'),
(117, 'Thailand', 'Baht', 'THB', '฿'),
(118, 'Trinidad and Tobago', 'Dollars', 'TTD', 'TT$'),
(119, 'Turkey', 'Lira', 'TRY', 'TL'),
(120, 'Turkey', 'Liras', 'TRL', '₺'),
(121, 'Tuvalu', 'Dollars', 'TVD', '$'),
(122, 'Ukraine', 'Hryvnia', 'UAH', '₴'),
(123, 'United Kingdom', 'Pounds', 'GBP', '£'),
(124, 'United States of America', 'Dollars', 'USD', '$'),
(125, 'Uruguay', 'Pesos', 'UYU', '$U'),
(126, 'Uzbekistan', 'Sums', 'UZS', 'so\'m'),
(127, 'Vatican City', 'Euro', 'EUR', '€'),
(128, 'Venezuela', 'Bolivares Fuertes', 'VEF', 'Bs'),
(129, 'Vietnam', 'Dong', 'VND', '₫\r\n'),
(130, 'Yemen', 'Rials', 'YER', '﷼'),
(131, 'Zimbabwe', 'Zimbabwe Dollars', 'ZWD', 'Z$');

-- --------------------------------------------------------

--
-- Table structure for table `employee_detail`
--

CREATE TABLE `employee_detail` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` text,
  `service` text,
  `icon` varchar(255) NOT NULL DEFAULT 'default.png',
  `status` int(11) NOT NULL DEFAULT '1',
  `experience` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `app_user_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_detail`
--

INSERT INTO `employee_detail` (`id`, `emp_id`, `address`, `description`, `service`, `icon`, `status`, `experience`, `created_at`, `updated_at`, `app_user_id`, `branch_id`) VALUES
(1, 8, '320 Oxford St,  Paddington NSW 2021', 'A high-fashion stylist and educator with 17 years of experience under her belt', '3,1,2', '5ed793d46fbd5.jpg', 1, '10 Year', '2020-06-03 17:43:08', '2020-06-03 17:43:08', 0, 0),
(2, 9, 'Wasaga Beach ON, Canada', 'From colouring and cutting to styling, Zoia is an all-rounder', '7,9,8', '5ed79424be04a.jpg', 1, 'Art Director & Senior Colour Specialist', '2020-06-03 17:44:28', '2020-06-03 17:44:28', 0, 0),
(3, 10, 'Miami Beach FL, USA', 'After completing his apprenticeship in Adelaide,', '4,6,5', '5ed7947723826.jpg', 1, 'Art Director & Senior Colour Specialist', '2020-06-03 17:45:51', '2020-06-03 17:45:51', 0, 0),
(4, 16, 'No Address', 'No Description', '7,9,4', '603a26ac95f03.png', 1, '4', '2021-02-27 16:32:04', '2021-02-27 16:32:04', 0, 0),
(5, 17, 'HOuse 1233 Branch Nill', NULL, NULL, 'default.png', 0, NULL, '2021-03-01 13:28:50', '2021-03-01 13:28:50', 28, 1);

-- --------------------------------------------------------

--
-- Table structure for table `emp_jobs`
--

CREATE TABLE `emp_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `branch_name` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_started_at` datetime DEFAULT NULL,
  `cancel_at` datetime DEFAULT NULL,
  `reached_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `started_longitude` decimal(13,10) DEFAULT NULL,
  `started_latitude` decimal(13,10) DEFAULT NULL,
  `target_longitude` decimal(13,10) DEFAULT NULL,
  `target_latitude` decimal(13,10) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Job started, 2 = Reached, 3 = Completed, 4 = Cancel',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 =>not payed yet, 1 => In process, 2 => Succeeded, 3 => canceled, 4 => payment failed	',
  `payment_intent_id` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Id for creating order payment'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emp_jobs`
--

INSERT INTO `emp_jobs` (`id`, `employee_email`, `employee_user_id`, `branch_id`, `branch_name`, `booking_id`, `user_id`, `user_email`, `user_name`, `job_started_at`, `cancel_at`, `reached_at`, `completed_at`, `started_longitude`, `started_latitude`, `target_longitude`, `target_latitude`, `status`, `created_at`, `updated_at`, `payment_status`, `payment_intent_id`) VALUES
(10, 'employeeuser_2@gmail.com', 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '2021-03-06 18:24:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(3, 'default', '{\"uuid\":\"454ae538-5701-4cde-88c2-b8efeadd1170\",\"displayName\":\"App\\\\Jobs\\\\SendEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendEmail\",\"command\":\"O:18:\\\"App\\\\Jobs\\\\SendEmail\\\":9:{s:26:\\\"\\u0000App\\\\Jobs\\\\SendEmail\\u0000record\\\";a:7:{s:10:\\\"first_name\\\";s:10:\\\"Emp User 2\\\";s:3:\\\"otp\\\";s:4:\\\"9502\\\";s:8:\\\"to_email\\\";s:24:\\\"employeeuser_2@gmail.com\\\";s:10:\\\"from_email\\\";s:29:\\\"info@wondrous-enterprises.com\\\";s:5:\\\"title\\\";s:20:\\\"MY Agnes Application\\\";s:7:\\\"subject\\\";s:17:\\\"MY Agnes OTP Code\\\";s:10:\\\"blade_file\\\";s:9:\\\"email.otp\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2021-03-01 13:28:52.327249\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:13:\\\"Asia\\/Calcutta\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1614585532, 1614585530);

-- --------------------------------------------------------

--
-- Table structure for table `job_location_history`
--

CREATE TABLE `job_location_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_id` int(11) NOT NULL,
  `employee_user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `branch_name` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` decimal(19,10) DEFAULT NULL,
  `latitude` decimal(19,10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_location_history`
--

INSERT INTO `job_location_history` (`id`, `job_id`, `employee_user_id`, `branch_id`, `branch_name`, `booking_id`, `user_id`, `user_email`, `user_name`, `longitude`, `latitude`, `created_at`, `updated_at`) VALUES
(1, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, NULL, '121312323.2320000000', NULL, NULL),
(2, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, NULL, '121312323.2320000000', NULL, NULL),
(3, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, NULL, '121312323.2320000000', NULL, NULL),
(4, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, NULL, '121312323.2320000000', NULL, NULL),
(5, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, NULL, '121312323.2320000000', NULL, NULL),
(6, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, NULL, '121312323.2320000000', NULL, NULL),
(7, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, NULL, '121312323.2320000000', NULL, NULL),
(8, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(9, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(10, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(11, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(12, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(13, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(14, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(15, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(16, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(17, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(18, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(19, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(20, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(21, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(22, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(23, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(24, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(25, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(26, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(27, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(28, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(29, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(30, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(31, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(32, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(33, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(34, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(35, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(36, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(37, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(38, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(39, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(40, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(41, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(42, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(43, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(44, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(45, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(46, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(47, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(48, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(49, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(50, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(51, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(52, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(53, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(54, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(55, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(56, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL),
(57, 2, 28, 1, 1, 19, 16, 'usamaahm61@gmail.com', NULL, '22343.4540000000', '121.2324564000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 2),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(9, '2021_02_01_131843_alter_branch_table', 3),
(10, '2021_02_01_164442_create_settings_table', 4),
(11, '2021_02_06_142637_alter_userapp_table', 5),
(12, '2021_02_14_165203_create_cart_table', 6),
(13, '2021_02_14_165217_create_cart_items_table', 6),
(14, '2021_02_14_183102_alter_cart_items_table', 6),
(16, '2021_02_14_194941_alter_appuser_table', 7),
(17, '2021_02_14_234037_alter_cart_items_add_branch_table', 7),
(18, '2021_02_20_182702_alter_app_user_add_pin_code', 7),
(19, '2021_02_21_021412_alter_cart_items_table_add_checkout_status', 7),
(20, '2021_02_21_021517_create_checkout_table', 7),
(21, '2021_02_21_213722_alter_booking_child_table', 7),
(23, '2021_02_27_133134_alter_add_uniqueness_checkout_column', 8),
(24, '2021_02_27_152418_alter_booking_master_add_action_date_time', 9),
(25, '2021_02_27_173421_alter_employee_table_add_app_user_id', 10),
(26, '2021_02_27_174348_create_jobs_table', 11),
(27, '2021_02_28_025115_alter_app_user_add_access_token', 12),
(28, '2021_02_28_041259_create_table_user_sockets_table', 13),
(29, '2021_02_28_143016_alter_user_sockets_table_add_new_columns', 14),
(30, '2021_02_28_150931_jobs', 15),
(31, '2021_02_28_201131_alter_emp_jobs_add_uniqueness', 16),
(32, '2021_02_28_213700_alter_booking_child_table_add_columns', 17),
(33, '2021_03_01_110036_create_job_location_history', 18),
(34, '2021_03_01_131547_alter_employee_info_add_columns', 19),
(35, '2021_03_01_133042_alter_admin_settings_table', 20),
(36, '2021_03_01_204845_alter_app_user_table_add_admin_approval_column', 21),
(37, '2021_03_02_015407_alter_booking_child_table_add_total_column', 22),
(38, '2021_03_02_020728_alter_booking_child_table_add_column_status', 23),
(39, '2021_03_02_223925_create_payments_table', 24),
(42, '2021_03_02_231321_create_transaction_table', 25),
(44, '2021_03_03_130624_alter_employee_job_add_columns', 26),
(45, '2021_03_03_143315_alter_transaction_add_columns', 27),
(46, '2021_03_04_222656_create_notifications_table', 28),
(47, '2021_03_07_002938_alter_socket_user_table_add_geolocations', 29),
(49, '2021_03_07_205815_alter_table_notifications', 30),
(50, '2021_03_11_234616_alter_all_tables_having_datetime', 31),
(51, '2021_03_16_173827_alter_app_users_add_social_login_fields', 31),
(52, '2021_04_22_130418_create_auth_info_table', 32);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sender_user_id` int(11) NOT NULL,
  `sender_user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reciever_user_id` int(11) NOT NULL,
  `reciever_user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reciever_user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_record` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 = Services Booked, 2 = Worker Reached, 3 = Booking Completed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seen` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `sender_user_id`, `sender_user_name`, `sender_user_email`, `reciever_user_id`, `reciever_user_name`, `reciever_user_email`, `data_record`, `type`, `created_at`, `updated_at`, `seen`) VALUES
(1, 0, 'Administration', 'admin@admin.com', 28, 'Emp User 2', 'employeeuser_2@gmail.com', '{\"senderName\":\"Administration\",\"senderEmail\":\"admin@admin.com\",\"senderUserId\":0,\"recieverId\":28,\"recieverEmail\":\"Emp User 2\",\"recieverName\":\"employeeuser_2@gmail.com\",\"services\":[\"Professional Hair Wash\"],\"bookingId\":16,\"message\":\"You have been assigned a new booking scheduled on date 25\\/02\\/2021\",\"startTime\":\"2021-02-25 09:00:00\",\"endTime\":\"2021-02-25 09:00:00\",\"clientName\":\"Usama\",\"clientAddress\":\"124 D3 Wapda Town Lahore\"}', 5, '2021-03-06 19:00:00', '2021-03-07 16:27:11', 1),
(2, 0, 'Administration', 'admin@admin.com', 17, 'usama', 'usamaahm61+1@gmail.com', '{\"senderName\":\"Administration\",\"senderEmail\":\"admin@admin.com\",\"senderUserId\":0,\"recieverId\":17,\"recieverEmail\":\"usama\",\"recieverName\":\"usamaahm61+1@gmail.com\",\"services\":[null],\"bookingId\":5,\"message\":\"You have been assigned a new booking scheduled on date 04\\/06\\/2020\",\"startTime\":\"2020-06-04 20:00:00\",\"endTime\":\"2020-06-04 20:40:00\",\"clientName\":null,\"clientAddress\":\"\"}', 5, '2021-04-02 19:00:00', '2021-04-02 19:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `notification_tbl`
--

CREATE TABLE `notification_tbl` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `sub_title` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_tbl`
--

INSERT INTO `notification_tbl` (`id`, `booking_id`, `user_id`, `sender_id`, `title`, `sub_title`, `created_at`, `updated_at`) VALUES
(1, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:21', '2020-06-04 11:47:21'),
(2, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:23', '2020-06-04 11:47:23'),
(3, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:25', '2020-06-04 11:47:25'),
(4, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:26', '2020-06-04 11:47:26'),
(5, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:27', '2020-06-04 11:47:27'),
(6, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:28', '2020-06-04 11:47:28'),
(7, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:29', '2020-06-04 11:47:29'),
(8, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:30', '2020-06-04 11:47:30'),
(9, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:31', '2020-06-04 11:47:31'),
(10, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:32', '2020-06-04 11:47:32'),
(11, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:33', '2020-06-04 11:47:33'),
(12, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:34', '2020-06-04 11:47:34'),
(13, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:35', '2020-06-04 11:47:35'),
(14, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:36', '2020-06-04 11:47:36'),
(15, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:37', '2020-06-04 11:47:37'),
(16, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:37', '2020-06-04 11:47:37'),
(17, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:38', '2020-06-04 11:47:38'),
(18, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:39', '2020-06-04 11:47:39'),
(19, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:40', '2020-06-04 11:47:40'),
(20, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:42', '2020-06-04 11:47:42'),
(21, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:43', '2020-06-04 11:47:43'),
(22, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:44', '2020-06-04 11:47:44'),
(23, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:44', '2020-06-04 11:47:44'),
(24, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:45', '2020-06-04 11:47:45'),
(25, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:46', '2020-06-04 11:47:46'),
(26, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:47', '2020-06-04 11:47:47'),
(27, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:48', '2020-06-04 11:47:48'),
(28, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:49', '2020-06-04 11:47:49'),
(29, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:50', '2020-06-04 11:47:50'),
(30, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:51', '2020-06-04 11:47:51'),
(31, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:51', '2020-06-04 11:47:51'),
(32, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:52', '2020-06-04 11:47:52'),
(33, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:53', '2020-06-04 11:47:53'),
(34, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:54', '2020-06-04 11:47:54'),
(35, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:47:55', '2020-06-04 11:47:55'),
(36, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:16', '2020-06-04 11:48:16'),
(37, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:28', '2020-06-04 11:48:28'),
(38, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:29', '2020-06-04 11:48:29'),
(39, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:30', '2020-06-04 11:48:30'),
(40, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:31', '2020-06-04 11:48:31'),
(41, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:31', '2020-06-04 11:48:31'),
(42, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:32', '2020-06-04 11:48:32'),
(43, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:34', '2020-06-04 11:48:34'),
(44, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:34', '2020-06-04 11:48:34'),
(45, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:35', '2020-06-04 11:48:35'),
(46, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:36', '2020-06-04 11:48:36'),
(47, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:37', '2020-06-04 11:48:37'),
(48, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:38', '2020-06-04 11:48:38'),
(49, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:38', '2020-06-04 11:48:38'),
(50, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:39', '2020-06-04 11:48:39'),
(51, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:40', '2020-06-04 11:48:40'),
(52, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:41', '2020-06-04 11:48:41'),
(53, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:41', '2020-06-04 11:48:41'),
(54, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:42', '2020-06-04 11:48:42'),
(55, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:57', '2020-06-04 11:48:57'),
(56, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:58', '2020-06-04 11:48:58'),
(57, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:58', '2020-06-04 11:48:58'),
(58, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:48:59', '2020-06-04 11:48:59'),
(59, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:01', '2020-06-04 11:49:01'),
(60, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:02', '2020-06-04 11:49:02'),
(61, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:02', '2020-06-04 11:49:02'),
(62, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:03', '2020-06-04 11:49:03'),
(63, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:04', '2020-06-04 11:49:04'),
(64, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:05', '2020-06-04 11:49:05'),
(65, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:06', '2020-06-04 11:49:06'),
(66, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:06', '2020-06-04 11:49:06'),
(67, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:40', '2020-06-04 11:49:40'),
(68, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:41', '2020-06-04 11:49:41'),
(69, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:41', '2020-06-04 11:49:41'),
(70, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:42', '2020-06-04 11:49:42'),
(71, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:42', '2020-06-04 11:49:42'),
(72, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:43', '2020-06-04 11:49:43'),
(73, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:43', '2020-06-04 11:49:43'),
(74, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:44', '2020-06-04 11:49:44'),
(75, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:44', '2020-06-04 11:49:44'),
(76, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:45', '2020-06-04 11:49:45'),
(77, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:46', '2020-06-04 11:49:46'),
(78, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:46', '2020-06-04 11:49:46'),
(79, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:47', '2020-06-04 11:49:47'),
(80, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:49:47', '2020-06-04 11:49:47'),
(81, 2, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 11:50:14', '2020-06-04 11:50:14'),
(82, 3, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2020-06-04 13:09:18', '2020-06-04 13:09:18'),
(83, 2, 11, 1, 'Booking For {{farm_title}}  is cancel', 'Your Booking on {{check_in_date}}    is Cancel please many further information bhk-5ed890 keep this ref no.', '2020-06-04 13:11:07', '2020-06-04 13:11:07'),
(84, 7, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2021-02-27 16:23:23', '2021-02-27 16:23:23'),
(85, 6, 11, 1, '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', '2021-04-11 16:43:24', '2021-04-11 16:43:24'),
(86, 6, 11, 1, 'Booking For {{farm_title}}  is cancel', 'Your Booking on {{check_in_date}}    is Cancel please many further information bhk-5ed8c2 keep this ref no.', '2021-04-11 16:43:42', '2021-04-11 16:43:42'),
(87, 5, 11, 1, 'Dear test, {{farm_title}}  Just Reject Booking Req', 'tatatattatata', '2021-05-28 00:38:21', '2021-05-28 00:38:21'),
(88, 4, 11, 3, 'Dear test, {{farm_title}}  Just Reject Booking Req', 'tatatattatata', '2021-05-28 00:55:44', '2021-05-28 00:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0078c0924dbb8a8884c4d154fb7764d2e24cbf8cb70fa413a86ed5ba0907b865285f8de894c0ec1f', 16, 1, 'user', '[]', 0, '2021-02-24 00:33:02', '2021-02-24 00:33:02', '2022-02-24 00:33:02'),
('02247c80ddf13c75068af25ba53daed5136cb568bc217ba4a53fd830c14e4166fe982d6ed4440765', 14, 1, 'Workeasy', '[]', 0, '2021-02-06 17:43:56', '2021-02-06 17:43:56', '2022-02-06 17:43:56'),
('0545b126260925a44fca9d612aa2a1c1d6256fd2bc1a0aae5fce95cd697b608e3fa2cc391c811156', 9, 1, 'user', '[]', 0, '2020-05-22 23:44:43', '2020-05-22 23:44:43', '2021-05-23 05:14:43'),
('05575195ebcdf550276fabfea4e5a50c3f01d46cb886451a4cb879441933bb8ad58b47f6b62cb8e8', 28, 1, 'user', '[]', 0, '2021-03-01 21:28:34', '2021-03-01 21:28:34', '2022-03-02 02:28:34'),
('0579b9b5483cfcab007f29fd3c15d2068f66b875fb2f5e10787e36ed55cb04a47f9241fafeb87bfd', 16, 1, 'user', '[]', 0, '2021-02-24 23:56:51', '2021-02-24 23:56:51', '2022-02-24 23:56:51'),
('05b49b12c59ed340dad204fccc6b7cc7dab88b592649a9935f7de10eb4b1a16aaeb16619b45f7aa2', 11, 1, 'users', '[]', 0, '2020-05-25 04:17:09', '2020-05-25 04:17:09', '2021-05-25 09:47:09'),
('064846bd4802c2e4f12359469c216cbaf0f333a9fd6d2c584e13af45a96cb80595f8091ef63012a7', 28, 1, 'user', '[]', 0, '2021-03-07 16:15:32', '2021-03-07 16:15:32', '2022-03-07 21:15:32'),
('0def7198f2086628a52ac81667acebb3d68924d1c8c42d0a5cd5295b4e2e05068180a769c57dfa87', 17, 1, 'user', '[]', 0, '2021-02-14 17:34:14', '2021-02-14 17:34:14', '2022-02-14 17:34:14'),
('0efd66e68d0f03102f9f55fd6f7720aa9ca51c1d37139b5a870a69365e4addcbe38e78b894e720dc', 16, 1, 'user', '[]', 0, '2021-02-17 01:43:09', '2021-02-17 01:43:09', '2022-02-17 01:43:09'),
('0f2ed38a99104d8ec94483f536b6467544be3bfc20761ca14cf00e8429259a1c36fe73686353706e', 28, 1, 'user', '[]', 0, '2021-03-06 18:36:56', '2021-03-06 18:36:56', '2022-03-06 23:36:56'),
('0f83b8e70eec4473c9be815b2e285bef242824c6c40735b3dfbb3675c580345ca14a71ef1d053bcf', 16, 1, 'user', '[]', 0, '2021-02-21 01:57:20', '2021-02-21 01:57:20', '2022-02-21 01:57:20'),
('101942448100880a137dd4b84f14790e6b07df967906f35d1bfe99060595ed3177ddc9024f900a69', 11, 1, 'user', '[]', 0, '2021-02-06 14:21:06', '2021-02-06 14:21:06', '2022-02-06 14:21:06'),
('12afd638a16aa919a086bf8bbafd60a84f87922a2cff139a5ba106657b6fd65373b7b4c995547c1c', 14, 1, 'user', '[]', 0, '2021-03-17 18:45:12', '2021-03-17 18:45:12', '2022-03-17 23:45:12'),
('12e60c9b5d6ae53ffd0452b6e1227b3d0bd79ce81d0e282ce42d4612514f1bfce73374fa7cbc17b7', 16, 1, 'user', '[]', 0, '2021-02-16 23:05:06', '2021-02-16 23:05:06', '2022-02-16 23:05:06'),
('13903d518bb90999d57bd293748774605b4b5a0c4cb9b13576f74ec4bfa9bf42dfb4af06f5b6d3b1', 16, 1, 'user', '[]', 0, '2021-02-18 10:53:20', '2021-02-18 10:53:20', '2022-02-18 10:53:20'),
('141d456d23b1acccba79936b07e5a0dc42ab2d1a8bf6c13f077fffcb8f2e40c8197109d32f13fa59', 11, 1, 'user', '[]', 0, '2021-02-06 20:13:55', '2021-02-06 20:13:55', '2022-02-06 20:13:55'),
('163c0bb18baca0a3acdfcbfab447fff20b76d307fc7ba795e53bf55dcb88a02b675f6e563f9323c5', 16, 1, 'user', '[]', 0, '2021-02-18 01:54:06', '2021-02-18 01:54:06', '2022-02-18 01:54:06'),
('1916cbaa21b5e847abdac9409b8192d5a79ad929bd0019c3352c06dfbcecb0c5daa3df3fafbaf675', 13, 1, 'user', '[]', 0, '2021-02-06 16:20:16', '2021-02-06 16:20:16', '2022-02-06 16:20:16'),
('1aeb921f990a82af630e9f449862f8261a0ae6acec0ff029c4eb7a485f1dc0d2c707dcd535854cda', 16, 1, 'user', '[]', 0, '2021-02-26 01:40:33', '2021-02-26 01:40:33', '2022-02-26 01:40:33'),
('1ec900b19a56993b533fb81d57d963d92fd560f0e5aaab537e1b4298b86dba0977d561e690521a2d', 16, 1, 'user', '[]', 0, '2021-02-25 00:57:35', '2021-02-25 00:57:35', '2022-02-25 00:57:35'),
('210c38012ca74addb284f2a6315d920a1f2e880004feec390f32efba9859a1717e98ef076a0398ca', 11, 1, 'user', '[]', 0, '2021-02-06 20:02:19', '2021-02-06 20:02:19', '2022-02-06 20:02:19'),
('250a107c58036ff76e2839464560d8722d9a8f6c871c3ee2434916988350aebfc06823ea5aecdfb7', 16, 1, 'user', '[]', 0, '2021-02-20 16:08:04', '2021-02-20 16:08:04', '2022-02-20 16:08:04'),
('25d3aaa842e090c943c467bd6f77b365c88e6235c104d87c2659f933a00ce8bfd02f41e94b73196d', 16, 1, 'user', '[]', 0, '2021-02-14 13:04:10', '2021-02-14 13:04:10', '2022-02-14 13:04:10'),
('268f2d4899332709d97533e18b6df2fec1ed3971c877cf5fb11255a5f5b5bc069dc0eaaac18213c4', 11, 1, 'user', '[]', 0, '2021-02-06 18:35:57', '2021-02-06 18:35:57', '2022-02-06 18:35:57'),
('26fc4734b4fd3fe4acbbc4c53bb7e9631d5c7bf1fddb9fe691d13ec09414052c05aaf6d74983125b', 11, 1, 'users', '[]', 0, '2020-05-25 04:19:14', '2020-05-25 04:19:14', '2021-05-25 09:49:14'),
('2aaf49133aeb6bb5205aadcb8b9a400a4cf5cad99440c7a21dcd47807b629089a8a0304e01a94637', 14, 1, 'user', '[]', 0, '2021-03-16 13:59:15', '2021-03-16 13:59:15', '2022-03-16 18:59:15'),
('2c924e1c964ed6f75868c561b251ec25ee07d8da34cbb86850f3f7652abc1e1abafbf422d267bf4d', 11, 1, 'Workeasy', '[]', 0, '2020-05-25 04:10:33', '2020-05-25 04:10:33', '2021-05-25 09:40:33'),
('2f0b613c273275a29c3f6502486678ac03c94d607c977cd4260ab70b1b983dfae8cfc0fa1e9b772c', 11, 1, 'user', '[]', 0, '2021-02-06 18:34:46', '2021-02-06 18:34:46', '2022-02-06 18:34:46'),
('2f2401c80a8585ecae33254235d7a704503b56e8682236fc789ccd090184f58211989a2b92e9661b', 16, 1, 'user', '[]', 0, '2021-02-12 16:03:24', '2021-02-12 16:03:24', '2022-02-12 16:03:24'),
('30c045c2d94b21534fdf1da6db6d270208b56d17d43bc72751aa7a48fed5582c8d24b90b12402306', 14, 1, 'user', '[]', 0, '2021-02-06 19:58:38', '2021-02-06 19:58:38', '2022-02-06 19:58:38'),
('3153dfba0e18d12882ccd2339da1e0d79d98a3ddd643560db8d63f08ca0feaad43954fb81292e49d', 16, 1, 'user', '[]', 0, '2021-02-14 23:50:00', '2021-02-14 23:50:00', '2022-02-14 23:50:00'),
('31dfbe2a6394c8deb8fa5c6a8d2cf6623bee9cc882b4c16e549342b66876fa08863df9274690c338', 16, 1, 'user', '[]', 0, '2021-02-24 00:31:54', '2021-02-24 00:31:54', '2022-02-24 00:31:54'),
('34e1eddf412ad19d97f4b3596261f8b10ca6fcf2fc9bc2e45f2f4ebbae6d625552c9f653b9c2fd0a', 11, 1, 'Workeasy', '[]', 0, '2020-05-25 04:16:59', '2020-05-25 04:16:59', '2021-05-25 09:46:59'),
('376ea75ff0b99dfa21c92e71c9a213dbcf9ffadc86c9ce4e965b07e31b8c7e639229b5f1a777acc8', 11, 1, 'user', '[]', 0, '2020-05-25 01:47:39', '2020-05-25 01:47:39', '2021-05-25 07:17:39'),
('3b00ba56a2a5fbb33d51b928d6ad71cc71ef167755aab67460eca5b7cf640a4352fa1f6c9086cddb', 16, 1, 'user', '[]', 0, '2021-02-20 15:12:59', '2021-02-20 15:12:59', '2022-02-20 15:12:59'),
('3b63cc919c29ed43eb6890f8615be3d0b9894c9230ab06e4e6c5e2ee408f85e7350a15d224b10bf7', 14, 1, 'user', '[]', 0, '2021-03-16 13:59:28', '2021-03-16 13:59:28', '2022-03-16 18:59:28'),
('3b6ebbe2d88b0390cf22e870cdfce38c542dece7c3c7985185635b07b5336f46b72cb507fc7365ae', 16, 1, 'user', '[]', 0, '2021-02-27 21:56:34', '2021-02-27 21:56:34', '2022-02-28 02:56:34'),
('3d9542a80a739f78967f524a9928a35526da4a69a723f91a78bc1cd9794fba9b2c6b088291a4547b', 11, 1, 'user', '[]', 0, '2020-06-01 10:07:59', '2020-06-01 10:07:59', '2021-06-01 15:37:59'),
('3dc25933a1db8d87ad69e2390b6f3258ff97c7c9a3857c11c2842bb32c67722ca1c282b0fbc9d057', 28, 1, 'user', '[]', 0, '2021-03-01 10:57:17', '2021-03-01 10:57:17', '2022-03-01 15:57:17'),
('417c6180b344fc3f493d80c26ea6d023d1456631d39caca67ee83adf1e0bf5082e9dae32281ccd66', 16, 1, 'user', '[]', 0, '2021-02-26 02:31:43', '2021-02-26 02:31:43', '2022-02-26 02:31:43'),
('4394ad029a8ea3649cff72a66cff96ee0bec1d72f2fad202c91ef0e9066410f0049d73d92b38f9f3', 16, 1, 'user', '[]', 0, '2021-02-14 21:27:32', '2021-02-14 21:27:32', '2022-02-14 21:27:32'),
('4457b05f107642210d4a2b297276ec5f06a360ddeffb8ba495d37380ac6108b20a698c200e75da19', 16, 1, 'user', '[]', 0, '2021-02-14 21:50:37', '2021-02-14 21:50:37', '2022-02-14 21:50:37'),
('469296db702806b169d89f6125464f9a73bc6048d12ff757d2db6908b0415a5c59b9a178f75ff316', 11, 1, 'user', '[]', 0, '2021-02-06 20:47:07', '2021-02-06 20:47:07', '2022-02-06 20:47:07'),
('4961afb80e7143c5ffb90b58b26548e09b4fbd6d612c636c476a4f424256e4da4c692d4a1ea2fe80', 11, 1, 'user', '[]', 0, '2021-02-24 00:08:50', '2021-02-24 00:08:50', '2022-02-24 00:08:50'),
('50a9c0ccab6e8c58c1ef9aec477542ca2d27728be6da452e67780e0420020198e01709295b8a707f', 16, 1, 'user', '[]', 0, '2021-02-26 20:52:47', '2021-02-26 20:52:47', '2022-02-27 01:52:47'),
('516b85a31c80cf525b4b62228f40e6f2285ae59d270c410d511f1a4876918c44982c74e27e8dd000', 16, 1, 'user', '[]', 0, '2021-03-03 18:27:53', '2021-03-03 18:27:53', '2022-03-03 23:27:53'),
('551d6545417f5683bdcef412e2d075aa9209bcf78760ecb652746da9a1eac2ac9326e894417d626b', 17, 1, 'user', '[]', 0, '2021-02-14 17:33:18', '2021-02-14 17:33:18', '2022-02-14 17:33:18'),
('5b2a7e9d5ac7426703055ebe53d5fca22dd2229971dfbb59ad39dbc8a3739c8ff9caac48450419f4', 17, 1, 'user', '[]', 0, '2021-02-14 17:25:10', '2021-02-14 17:25:10', '2022-02-14 17:25:10'),
('5dd75ca64feea69b2f0857a44705277f3d3604a143639ba3320206e1d01c9165139cdbee3307622a', 16, 1, 'user', '[]', 0, '2021-03-03 07:58:47', '2021-03-03 07:58:47', '2022-03-03 12:58:47'),
('5f08c7ed1b20e8d04062f1256582e66abc6f38d8677eb9dfe99e67ed9cc4fcaac8d17f91b278605a', 11, 1, 'users', '[]', 0, '2020-05-25 04:15:36', '2020-05-25 04:15:36', '2021-05-25 09:45:36'),
('638dfcc15b7a4d9f19062d2bf7ddf3386bb919e4cc02fa102392a39d3f5568b8c098299c95b3c2f7', 14, 1, 'user', '[]', 0, '2021-03-16 19:27:03', '2021-03-16 19:27:03', '2022-03-17 00:27:03'),
('6579c9c3040c8f8b85c952f3444b049bad298d75ded8bb81d0e7f73005ae2177d31d96a7f78a7ec2', 16, 1, 'user', '[]', 0, '2021-02-14 17:07:51', '2021-02-14 17:07:51', '2022-02-14 17:07:51'),
('66e711e72f185298d3a946217d3fd4b2ad8f60b669136046d1dd45cc36934bc6b8c29f57637ae739', 16, 1, 'user', '[]', 0, '2021-02-24 00:08:59', '2021-02-24 00:08:59', '2022-02-24 00:08:59'),
('6708ea4f12b01bffd04c243a501a641a0664197eac64238b0a4f2dd1f49d4993870607a577c0a900', 16, 1, 'user', '[]', 0, '2021-02-14 17:11:26', '2021-02-14 17:11:26', '2022-02-14 17:11:26'),
('6ab5912a87240e84ce28347db8f25892acd5679049cf523d6c1e679e19bf721dad9c36cedc994a0d', 17, 1, 'user', '[]', 0, '2021-02-24 00:11:57', '2021-02-24 00:11:57', '2022-02-24 00:11:57'),
('6acee9977008f8da15b52673600a4cfc7c97fde36e30ea9b20edf1f8513fc646845a32f3d618442b', 11, 1, 'user', '[]', 0, '2021-02-06 20:03:11', '2021-02-06 20:03:11', '2022-02-06 20:03:11'),
('6afcdcb617245ed0a2694277be2439bbadc6ad7a51b8a705068c0de32d96c4255e9dca2770e62a20', 17, 1, 'user', '[]', 0, '2021-02-14 17:29:32', '2021-02-14 17:29:32', '2022-02-14 17:29:32'),
('6c8a2b052d72c91b0f2bc1a9d09c72516f593b5789b38209873c55a4054a104ccb01098ad7606920', 11, 1, 'user', '[]', 0, '2020-06-03 06:42:59', '2020-06-03 06:42:59', '2021-06-03 12:12:59'),
('72b915e5502304216163107a79804b8a4f666a6124747133a5d5ac2639c607947c14908b88996c8d', 16, 1, 'user', '[]', 0, '2021-02-27 21:56:29', '2021-02-27 21:56:29', '2022-02-28 02:56:29'),
('7478fa1b0b556e097e5270065d85237c17e263902f2aa6a3c451084bd7f3f516ea54f53c766eae9f', 11, 1, 'user', '[]', 0, '2021-02-01 11:05:23', '2021-02-01 11:05:23', '2022-02-01 16:05:23'),
('75a428177238d83aba9264d01fc9a66e052982b55824ef9e982e8886974ab39bd6a132a2c847d13a', 16, 1, 'user', '[]', 0, '2021-03-01 20:36:04', '2021-03-01 20:36:04', '2022-03-02 01:36:04'),
('7b6d0c3bf520c9c6af55923fb4be3149c0e46fcae31066e8e469273e838ad79ad269a2e08dfde3fd', 11, 1, 'user', '[]', 0, '2020-05-30 00:43:55', '2020-05-30 00:43:55', '2021-05-30 06:13:55'),
('7dfe5389254e11db1c6cb698e90c973a1badbe8cf96d07cf9c06b2ff210527d9ae329cf8a66ebff8', 17, 1, 'user', '[]', 0, '2021-02-14 17:28:44', '2021-02-14 17:28:44', '2022-02-14 17:28:44'),
('80a4602cbce414796a4b8af85d00a1266a1491ff351d41e7b9a6f24121dc6bfa64ea81a21c766807', 11, 1, 'user', '[]', 0, '2021-02-06 17:56:30', '2021-02-06 17:56:30', '2022-02-06 17:56:30'),
('827b741599c2c056d3aa5a08f8d5f1624a25d2a208cb5ef0c3b699374bf594563546aae1c37372fa', 11, 1, 'user', '[]', 0, '2020-05-25 01:19:48', '2020-05-25 01:19:48', '2021-05-25 06:49:48'),
('82874fa7ed3d9d277b00942048eb6900901f6a4c2780a557caead17938b535f4e18ce646db80e6dd', 28, 1, 'user', '[]', 0, '2021-03-16 19:33:33', '2021-03-16 19:33:33', '2022-03-17 00:33:33'),
('85b93d3b8d8007c2cd3bcc1f4ef35a95d1be230f3dc39593166fe62dbfddbff4a859f8fff0a5ffe1', 16, 1, 'user', '[]', 0, '2021-02-24 00:29:46', '2021-02-24 00:29:46', '2022-02-24 00:29:46'),
('86afd36c0991606ce4eb9b83403ee733f8d2ad2d61b5e878b3afbf5f046adc8d722d1fe2b9ef64fa', 12, 1, 'Workeasy', '[]', 0, '2020-05-25 03:51:03', '2020-05-25 03:51:03', '2021-05-25 09:21:03'),
('87e02567e99098e9940d6c4438b8fb34472f948a8e7c7bc4070449b3253cc5395ee7d021d105a11f', 11, 1, 'user', '[]', 0, '2020-05-25 04:17:49', '2020-05-25 04:17:49', '2021-05-25 09:47:49'),
('8877e2f5b69444ebf1fd6483fddc3098fbaa77e9d42baaef8ec51eeee7572813a55e3f94bea2db43', 11, 1, 'user', '[]', 0, '2020-06-04 06:07:06', '2020-06-04 06:07:06', '2021-06-04 11:37:06'),
('88fc99e58aeff25fedb29daccdcec6eadc0e0cc0da6adc57e92eb9b561ffa1c33b06b1288cf652b1', 11, 1, 'Workeasy', '[]', 0, '2020-05-25 04:18:37', '2020-05-25 04:18:37', '2021-05-25 09:48:37'),
('890bcebd59abe3b7b596ebfbfe58b9b9456c286b70e578659e2b1b3c43028968704362389e42c764', 17, 1, 'user', '[]', 0, '2021-02-14 23:23:57', '2021-02-14 23:23:57', '2022-02-14 23:23:57'),
('8aa5367db14d7e68231ee0ff3483c34d23fddfa24343ada7175c925174c92fe600c79c95970075b0', 11, 1, 'user', '[]', 0, '2021-02-06 18:31:48', '2021-02-06 18:31:48', '2022-02-06 18:31:48'),
('8d8c59f3effbf575efd42ba0ad291c4aad3fbeda749897c343fb8535c258abe7b52fc5eaab5f1c64', 16, 1, 'user', '[]', 0, '2021-02-26 02:16:17', '2021-02-26 02:16:17', '2022-02-26 02:16:17'),
('8efd70fcbbf81e1cf915c25f434222a13c2f5312665ef12eb45c3848e2cd89f23380bddd6193450f', 16, 1, 'user', '[]', 0, '2021-02-25 00:43:36', '2021-02-25 00:43:36', '2022-02-25 00:43:36'),
('91951eef9ab8e71e0411e82bda98804e9f60989958ac5252c7aaa630afaedca7f9159db5b4c3c587', 14, 1, 'user', '[]', 0, '2021-03-16 13:57:28', '2021-03-16 13:57:28', '2022-03-16 18:57:28'),
('9378d7dbd880f98ba9910475810e97a573749b8945064ef0a6422a37e8e978b5ea166292e93ec239', 11, 1, 'user', '[]', 0, '2020-06-02 09:21:49', '2020-06-02 09:21:49', '2021-06-02 14:51:49'),
('94eb34dcba19b6154c5919c0f8bcf6ee72b6cd3fccfec09defea6b7bc215b4265994b2a7c4e4d7b1', 11, 1, 'user', '[]', 0, '2021-02-14 16:45:04', '2021-02-14 16:45:04', '2022-02-14 16:45:04'),
('96c8c250e3a3edaf759cfd4133d3e4d1bc3667466b98e11e2451d0906582296fe72ca4fce8bcec66', 14, 1, 'user', '[]', 0, '2021-03-16 19:36:25', '2021-03-16 19:36:25', '2022-03-17 00:36:25'),
('984d06064b3e3fff12002c2007a17cd180662f56df64b6607881c195628ebbce15270f0863ded1ff', 16, 1, 'user', '[]', 0, '2021-02-26 01:40:05', '2021-02-26 01:40:05', '2022-02-26 01:40:05'),
('985ac8752e427fa9e7be9c90f13f446ee8aa98b1817976ad68a18bd1d7e4433ab7727fa0f0a1b035', 11, 1, 'user', '[]', 0, '2021-02-06 20:16:42', '2021-02-06 20:16:42', '2022-02-06 20:16:42'),
('9d1fbbfdae27975f220430a256587cfa93daef02b9ea9931a2d9d8e01a6f7e3647550dab1748431a', 17, 1, 'user', '[]', 0, '2021-02-17 02:02:30', '2021-02-17 02:02:30', '2022-02-17 02:02:30'),
('9fcb95dfcba4b793c38f9b3b863719a2e4165bd303c65c3c2e3ec6027e41883199a29e6a8f7ba60c', 11, 1, 'user', '[]', 0, '2021-02-13 13:26:28', '2021-02-13 13:26:28', '2022-02-13 13:26:28'),
('a2e60715c9e9fe70110f4049c4358ae0ff57194d4c3b19bde1bf211fc8340af62933e047268678ad', 10, 1, 'user', '[]', 0, '2020-05-23 03:40:10', '2020-05-23 03:40:10', '2021-05-23 09:10:10'),
('a359f7f4410c205d6cd9bf20110225146b6795967b1fcf16d98df5ebb191592fe328367c716ae1c5', 11, 1, 'user', '[]', 0, '2020-05-25 03:47:16', '2020-05-25 03:47:16', '2021-05-25 09:17:16'),
('a470d4454928a979ed9724d54e73a9a0374c36bac1ea1a3d13a42eb4f2a0d5e2dacd0009f161b17a', 21, 1, 'user', '[]', 0, '2021-02-24 00:11:21', '2021-02-24 00:11:21', '2022-02-24 00:11:21'),
('a5c4d3f74c193b02f9029aef072ef2818b6d19689dc8dc837d1d36fdf79afc51fe7c9049e2527eb4', 16, 1, 'user', '[]', 0, '2021-02-24 00:22:30', '2021-02-24 00:22:30', '2022-02-24 00:22:30'),
('a658adde77c6aade0e0a56bd879db42491ce14565f40bd1200619048047308e14c8992044b585181', 11, 1, 'user', '[]', 0, '2021-02-14 21:18:33', '2021-02-14 21:18:33', '2022-02-14 21:18:33'),
('a9afd2e13f15eca153ee6681d0c2161929449c06967f0e39350f57a9f699196a8465243d1c63bf4e', 16, 1, 'user', '[]', 0, '2021-02-25 00:29:42', '2021-02-25 00:29:42', '2022-02-25 00:29:42'),
('acc94038e91e379996680fc0ead48ec2f7c3b6d45692436008ce47994829cedd8f84971729a87efd', 22, 1, 'user', '[]', 0, '2021-02-27 09:04:12', '2021-02-27 09:04:12', '2022-02-27 14:04:12'),
('b0dfd4df87677ba00a4c3d594bc458aa895b77f91375c88d4e815fd75ac8bc61937920dbcb666a2d', 11, 1, 'user', '[]', 0, '2021-02-06 18:35:12', '2021-02-06 18:35:12', '2022-02-06 18:35:12'),
('b197c7f67ba5a41c2d9f0c6b0112ee642a76d7484ad886eb7ee7723f98b0e4f3d8cfae11ce86e480', 16, 1, 'user', '[]', 0, '2021-02-26 02:32:30', '2021-02-26 02:32:30', '2022-02-26 02:32:30'),
('b2ea7b4d1a9af5cd5a46a772ff91fd202d2f4def8211bad5277d7d0cfd7d9e13a9993fbc00a41fdf', 11, 1, 'user', '[]', 0, '2020-05-25 03:31:39', '2020-05-25 03:31:39', '2021-05-25 09:01:39'),
('b3df0daef017ba7724ae36b8f8296a7dc2fbded1cfa689e7ff562e8aa664c79055e1f8c2d6e320e6', 28, 1, 'user', '[]', 0, '2021-03-06 18:36:52', '2021-03-06 18:36:52', '2022-03-06 23:36:52'),
('b4a0117b15d8c162cbc0f1753929db4616046cecb013caeefe7112bf3140e0ad7f3f9cf4692536e3', 11, 1, 'user', '[]', 0, '2021-01-30 14:08:52', '2021-01-30 14:08:52', '2022-01-30 19:08:52'),
('b525bf9f6d2a8c50eb90d45d3e84382c5a2cff87592be84031e178bf244b0094416c2483c5255313', 11, 1, 'user', '[]', 0, '2020-05-25 01:51:08', '2020-05-25 01:51:08', '2021-05-25 07:21:08'),
('b54355d4f74fb43e00a57cf955f47aa692f706be43278f0cce8a9246a85b6b7d4439bfa830482989', 11, 1, 'user', '[]', 0, '2021-02-26 18:41:05', '2021-02-26 18:41:05', '2022-02-26 23:41:05'),
('b614083dc241f384518969a6dc3ad77c5c6dfc90ddf9fdd19361cad9ec4614b2ac9c740b3223ec18', 11, 1, 'user', '[]', 0, '2021-02-14 21:16:52', '2021-02-14 21:16:52', '2022-02-14 21:16:52'),
('babf818b90a0f3879075b528e214158dc5d83744117f987f0cef2c8111049663e4bcecafb839359c', 16, 1, 'user', '[]', 0, '2021-02-14 17:29:55', '2021-02-14 17:29:55', '2022-02-14 17:29:55'),
('bb5167340c45ca374142eec9035f1bada925b1f95719cede944bd9a205abc60001d725a9fa20ae46', 11, 1, 'user', '[]', 0, '2020-06-03 06:25:13', '2020-06-03 06:25:13', '2021-06-03 11:55:13'),
('c430809f5534e4f52f857ff493be5fb869d24f4384cf2305247b42f4708cc765540151b5c136fd38', 16, 1, 'user', '[]', 0, '2021-02-06 19:40:13', '2021-02-06 19:40:13', '2022-02-06 19:40:13'),
('c5a3abd20547557a5a445b2f9c22384cdce15fce919e406beadd603d0531ef6aed5fc8b06e7448db', 11, 1, 'user', '[]', 0, '2020-06-02 09:46:45', '2020-06-02 09:46:45', '2021-06-02 15:16:45'),
('c6513370ab7814b97a962995407939d007f5aee658021d2a2902934be64db8be08bd8852b8b32545', 11, 1, 'user', '[]', 0, '2021-02-06 18:35:27', '2021-02-06 18:35:27', '2022-02-06 18:35:27'),
('c7f13f55bf2b342874d4e651e0ecd021e71c8e86f12c71aa3266af13de32f18343a162b9a43692dd', 16, 1, 'user', '[]', 0, '2021-02-24 00:28:40', '2021-02-24 00:28:40', '2022-02-24 00:28:40'),
('c8d9566922aaed36e05a189773eb749467ddf0f10ac7891dc70730f441679598ff63e4338c8567c7', 16, 1, 'user', '[]', 0, '2021-02-27 21:55:25', '2021-02-27 21:55:25', '2022-02-28 02:55:25'),
('c9ddbe1c1cf3a9b8dd2f4f3e9578f51c8a33fd8f32a25772f5a4397f1affb9330cae29e1309f2c22', 14, 1, 'user', '[]', 0, '2021-02-06 17:24:54', '2021-02-06 17:24:54', '2022-02-06 17:24:54'),
('ca0daec7bb839c4356e0e5fe6a3a10b7595abe3ef47ffe3a1193fefce7cea5e259acac44e1fb0e6a', 11, 1, 'user', '[]', 0, '2020-05-25 00:52:00', '2020-05-25 00:52:00', '2021-05-25 06:22:00'),
('cad917d548cec4eeac2ce4bff4b29190a36db063b5ad90d97b19405f27464a83e2e47a1880246758', 13, 1, 'user', '[]', 0, '2021-02-06 15:21:00', '2021-02-06 15:21:00', '2022-02-06 15:21:00'),
('cc1a78fac9714b1e71382a2c8d135d35a23f889da71c383a070a7f260e96a316e6411d8c77a656bd', 16, 1, 'user', '[]', 0, '2021-02-11 11:37:18', '2021-02-11 11:37:18', '2022-02-11 11:37:18'),
('cea5c58dab3a7b7ec4c01a98c7e1f7951086e257d3fe9336f3be10faf9e1d598d06add9c10b81d19', 16, 1, 'user', '[]', 0, '2021-02-26 02:15:41', '2021-02-26 02:15:41', '2022-02-26 02:15:41'),
('cf4f9f6348f94a62b11cd94c3ce7d45a5e812258a8203cc877da692057d5596af77c5197439e409b', 17, 1, 'user', '[]', 0, '2021-02-14 23:49:04', '2021-02-14 23:49:04', '2022-02-14 23:49:04'),
('d07c74b450dfede13c1204c08ca2d25b12dba7994075ca6dddd085ad2e2a01cf38a74e1fa1137ad5', 21, 1, 'user', '[]', 0, '2021-02-24 00:11:06', '2021-02-24 00:11:06', '2022-02-24 00:11:06'),
('d1d697b86f748e16f94583624f8c1aab71fe690289a036fe7833881eafea8446ea93e03c43094d89', 11, 1, 'user', '[]', 0, '2020-06-02 09:45:06', '2020-06-02 09:45:06', '2021-06-02 15:15:06'),
('d1f6928909136a6d012bf152d5dd1a654d4728ad1a0dbee7410095d4b81eb04d7ec7a6409b355099', 11, 1, 'user', '[]', 0, '2021-02-14 12:58:22', '2021-02-14 12:58:22', '2022-02-14 12:58:22'),
('d389ed7af0d25e8f85cf22e1b1a513b0e6062cee1cb9168241c226a6a6c2c9c3fd79e0610da5316d', 16, 1, 'user', '[]', 0, '2021-02-25 01:58:58', '2021-02-25 01:58:58', '2022-02-25 01:58:58'),
('d39ad6363764181a15c06aec9014a0d9ac203df6fe0c244dbe7714106b1c757eb5f2c81091400de8', 16, 1, 'user', '[]', 0, '2021-02-14 23:51:52', '2021-02-14 23:51:52', '2022-02-14 23:51:52'),
('d6496747f08a525e49a3ee17b23a7eb3b16a022cdc98dba2f1e4a15db8dce402ce02e216119c095d', 16, 1, 'user', '[]', 0, '2021-02-20 02:40:56', '2021-02-20 02:40:56', '2022-02-20 02:40:56'),
('d68d5e06bf76e7908f8fbd12255f9e128686a7707794bad10871ae3b0360ea94378758f5166f6e24', 21, 1, 'user', '[]', 0, '2021-02-24 00:12:42', '2021-02-24 00:12:42', '2022-02-24 00:12:42'),
('d983972ac476dd5ce3efc3f8ba8d9279c162240fd7ce7417e1a99643d6b0d8c1f3434a6794f4f6e3', 16, 1, 'user', '[]', 0, '2021-02-18 10:56:19', '2021-02-18 10:56:19', '2022-02-18 10:56:19'),
('d9e44f6fd0ec9b027a5471f814cd431a1a33870572e8ef2dcfc0afa3d900a321eea62e94fbcdb19c', 16, 1, 'user', '[]', 0, '2021-02-14 21:58:24', '2021-02-14 21:58:24', '2022-02-14 21:58:24'),
('da95087209a0fe7fd9f7eddbc64fa7b99eca31064bbfb46cc90a3254eeca7b893a77ee6af365d0dc', 14, 1, 'user', '[]', 0, '2021-02-24 00:09:18', '2021-02-24 00:09:18', '2022-02-24 00:09:18'),
('da966ef3f7d83b76e1ba0907a1110579e39f59689ce1a2cd6579084f67e5a1df5cfea178c8c46dcd', 11, 1, 'user', '[]', 0, '2021-02-06 20:31:33', '2021-02-06 20:31:33', '2022-02-06 20:31:33'),
('dba5d45dd027b9c7d72a21bcbe8a16041282847967a6142cea6805696affbe8f28d81a8938a5f4fa', 28, 1, 'user', '[]', 0, '2021-03-16 19:33:35', '2021-03-16 19:33:35', '2022-03-17 00:33:35'),
('dbf37b65de5e00243482337065de846ec7ddabc285e28d2a9bf8aa504ba13c6ac8d462345a2edb0e', 11, 1, 'user', '[]', 0, '2021-02-06 18:31:18', '2021-02-06 18:31:18', '2022-02-06 18:31:18'),
('dc79d38b78c7390ad138d6d7ca69a30d6e1fe21a048a657eff5304cc7f8e0209d0b25c8b0ef4a605', 14, 1, 'user', '[]', 0, '2021-03-16 19:35:31', '2021-03-16 19:35:31', '2022-03-17 00:35:31'),
('ddc8c6f55d551892d183e6dbc14aa9bc18a5a6a95b6a8026f708d45bbe3d1e21bd7c098e3111fedf', 11, 1, 'user', '[]', 0, '2021-02-06 14:18:43', '2021-02-06 14:18:43', '2022-02-06 14:18:43'),
('de048f8fce5907c534ced05ad1cb12c90fe023ed0994bc310d022010e22086d309831068359b7e82', 16, 1, 'user', '[]', 0, '2021-03-06 18:48:59', '2021-03-06 18:48:59', '2022-03-06 23:48:59'),
('ded9e044b601381a2a4d016aba81171bcdf8634d51b1a00b21aeaedd3b70332226719038483044b5', 12, 1, 'user', '[]', 0, '2020-05-25 03:49:26', '2020-05-25 03:49:26', '2021-05-25 09:19:26'),
('dfc4d8f251853140e0aba9944d9be0d9b6ddc9252f60c4a4af0f18abcb3f0b715f0bb60181421dc8', 28, 1, 'user', '[]', 0, '2021-03-01 10:57:25', '2021-03-01 10:57:25', '2022-03-01 15:57:25'),
('e2106937c10a71ee117eb0d58f2b7b42c734ef2574af9e71a135e06135f8fa3902063db0ca12724a', 16, 1, 'user', '[]', 0, '2021-03-03 18:02:07', '2021-03-03 18:02:07', '2022-03-03 23:02:07'),
('e28a342e28f0679f989e0d3316ebadedf1b287f68f1a4ff644f3f4892cc0ff36cf236b7bbe45f3ca', 16, 1, 'user', '[]', 0, '2021-02-18 23:37:47', '2021-02-18 23:37:47', '2022-02-18 23:37:47'),
('e33cb95bfbfe0f5e2b81a49f40dc907176c4bda33a1aa7364f080a868c263789c2b9878e4d85c676', 28, 1, 'user', '[]', 0, '2021-03-01 10:57:28', '2021-03-01 10:57:28', '2022-03-01 15:57:28'),
('e3e0e1a11518f31d55af7bc11d54f196b418d615b77c4a09fddc0f7a05fee6cfd7617db7091d9f2f', 11, 1, 'user', '[]', 0, '2021-02-06 17:58:43', '2021-02-06 17:58:43', '2022-02-06 17:58:43'),
('e4a09e6a03368ae63c87f1f52ea029ad818b3a3cd851e6b19d082b54908269d8696c3d10619fc00b', 17, 1, 'user', '[]', 0, '2021-02-14 17:36:11', '2021-02-14 17:36:11', '2022-02-14 17:36:11'),
('e4a73d2bd562911754d8fffe70eff4f9d603bca2bbc3bbd016c0ca0cb7c289f766d64f5cacd1812e', 16, 1, 'user', '[]', 0, '2021-02-24 00:30:07', '2021-02-24 00:30:07', '2022-02-24 00:30:07'),
('e6d161ab460d4a1c11b04dcf76977adc676373959761bc06aa648e977acb627561337afe9f46fe26', 16, 1, 'user', '[]', 0, '2021-02-24 00:30:56', '2021-02-24 00:30:56', '2022-02-24 00:30:56'),
('e8ece925afb59775e3254256c141a23d3123080a90c49ce0edb81f0edde6152df3d65d88563a8921', 11, 1, 'Workeasy', '[]', 0, '2020-05-25 04:14:29', '2020-05-25 04:14:29', '2021-05-25 09:44:29'),
('eb8163a1d9e911daf834ffe7488a92dd2ba3cb0e3165307b645f50ab26c9ec66a3b358e8902eedc7', 16, 1, 'user', '[]', 0, '2021-02-27 22:10:11', '2021-02-27 22:10:11', '2022-02-28 03:10:11'),
('ebbc92c66a6a30cda152481470ce711fd6a9434f53bbb245ace7c72e7a91c3276819ed039c3e9369', 11, 1, 'user', '[]', 0, '2021-02-06 18:32:21', '2021-02-06 18:32:21', '2022-02-06 18:32:21'),
('f1160605b5f2caf3f14c933ddd609990b69027e575954c54a8337ddd05ce1dc85f5c9cb8f9dde5d9', 16, 1, 'user', '[]', 0, '2021-02-23 23:48:58', '2021-02-23 23:48:58', '2022-02-23 23:48:58'),
('f3fd76d96475f157e9fef61a9d7bab1cebefbc3a70896beb562202baf67ccfd69d1cfca6abcb5fd3', 16, 1, 'user', '[]', 0, '2021-02-14 17:01:40', '2021-02-14 17:01:40', '2022-02-14 17:01:40'),
('f4cab47b519009641f0658b269515d69bb3e08c00fb8873f58a4f4a819c86bf28f32a7cff7c7e6a4', 16, 1, 'user', '[]', 0, '2021-02-06 19:40:23', '2021-02-06 19:40:23', '2022-02-06 19:40:23'),
('f5770ab057d38b1fc18191d7b52390772f0547a7c2dbcd9a4bdbc79b6615354d79d132988deea698', 21, 1, 'user', '[]', 0, '2021-02-24 00:12:04', '2021-02-24 00:12:04', '2022-02-24 00:12:04'),
('f9cd3cecbcebd1b64649d74790534a752ce4d9008745327da29b2290a1369f91e03f68da76b265c1', 11, 1, 'user', '[]', 0, '2021-02-06 18:32:05', '2021-02-06 18:32:05', '2022-02-06 18:32:05'),
('fa992d35c3bbff7016f997f0a3796fe5b503cb774f4fe69242c842ee93c36557369b460d2d0b993b', 11, 1, 'Workeasy', '[]', 0, '2020-05-25 04:07:00', '2020-05-25 04:07:00', '2021-05-25 09:37:00'),
('fc3a9628bc10e01524529a955863b64bff17dd1ad5d65ea02e88cd50b24fafb4f3e9faa97e42c6f3', 17, 1, 'user', '[]', 0, '2021-02-14 17:24:39', '2021-02-14 17:24:39', '2022-02-14 17:24:39'),
('fd6b66afa07836359743fb072566b8b28212599bd3a60f9edfbfb85963315bc0b766351509109ea5', 17, 1, 'user', '[]', 0, '2021-02-17 01:56:10', '2021-02-17 01:56:10', '2022-02-17 01:56:10'),
('fdb8f78e1b41eb9060f69f861ec136a7c90c474abdff09048eb199ab15e9652dbeba5b3ed52d7208', 17, 1, 'user', '[]', 0, '2021-02-14 17:35:32', '2021-02-14 17:35:32', '2022-02-14 17:35:32'),
('fe771fc74329ec98a17dc49272d3df8975373e78e0663cd39bc7b1bcbe8e3662632ae55fb107c72f', 16, 1, 'user', '[]', 0, '2021-02-14 17:07:51', '2021-02-14 17:07:51', '2022-02-14 17:07:51');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'beatybella Personal Access Client', 'dekhOdyeIA6QIj1rYOHhYH0tGrfJiqMsNkEMxXKG', NULL, 'http://localhost', 1, 0, 0, '2020-05-22 23:12:05', '2020-05-22 23:12:05'),
(2, NULL, 'beatybella Password Grant Client', 'NEtwr0hheZN2No5pEMlV4X3KfDYm0cV4wVTBwxtv', 'users', 'http://localhost', 0, 1, 0, '2020-05-22 23:12:05', '2020-05-22 23:12:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-22 23:12:05', '2020-05-22 23:12:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `branch_id` text,
  `title` varchar(255) NOT NULL,
  `how_expire` int(11) DEFAULT '0' COMMENT '0 = date 1 use',
  `expiry_date` date DEFAULT NULL,
  `max_usage` int(11) DEFAULT '-1',
  `max_use_user` int(11) DEFAULT '-1' COMMENT '-1 = unlimited',
  `min_amount` int(11) DEFAULT '-1' COMMENT '-1 = 0',
  `discount_type` int(11) DEFAULT '0' COMMENT '0 = amiount 1 = per',
  `discount` float DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0 = deactive 1 active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_method_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payment_method_code`, `payment_method_name`, `created_at`, `updated_at`) VALUES
(3, 'strp', 'Stripe', '2021-03-01 19:00:00', '2021-03-01 19:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dashboard', NULL, NULL, NULL),
(2, 'role_access', NULL, NULL, NULL),
(3, 'role_create', NULL, NULL, NULL),
(4, 'role_edit', NULL, NULL, NULL),
(5, 'role_show', NULL, NULL, NULL),
(6, 'role_delete', NULL, NULL, NULL),
(7, 'user_access', NULL, NULL, NULL),
(8, 'user_create', NULL, NULL, NULL),
(9, 'user_edit', NULL, NULL, NULL),
(10, 'user_show', NULL, NULL, NULL),
(11, 'user_delete', NULL, NULL, NULL),
(12, 'category_access', NULL, NULL, NULL),
(13, 'category_create', NULL, NULL, NULL),
(14, 'category_edit', NULL, NULL, NULL),
(15, 'category_show', NULL, NULL, NULL),
(16, 'category_delete', NULL, NULL, NULL),
(17, 'subcategory_access', NULL, NULL, NULL),
(18, 'subcategory_create', NULL, NULL, NULL),
(19, 'subcategory_edit', NULL, NULL, NULL),
(20, 'subcategory_show', NULL, NULL, NULL),
(21, 'subcategory_delete', NULL, NULL, NULL),
(38, 'earning_access', NULL, NULL, NULL),
(39, 'earning_show', NULL, NULL, NULL),
(40, 'earning_settle', NULL, NULL, NULL),
(41, 'notification_access', NULL, NULL, NULL),
(42, 'notification_edit', NULL, NULL, NULL),
(43, 'custom_notification_access', NULL, NULL, NULL),
(44, 'report_access', NULL, NULL, NULL),
(45, 'setting_access', NULL, NULL, NULL),
(46, 'privacy_access', NULL, NULL, NULL),
(47, 'privacy_edit', NULL, NULL, NULL),
(48, 'faq_access', NULL, NULL, NULL),
(49, 'faq_create', NULL, NULL, NULL),
(50, 'faq_edit', NULL, NULL, NULL),
(51, 'faq_show', NULL, NULL, NULL),
(52, 'faq_delete', NULL, NULL, NULL),
(53, 'lang_access', NULL, NULL, NULL),
(54, 'lang_create', NULL, NULL, NULL),
(55, 'lang_edit', NULL, NULL, NULL),
(56, 'lang_show', NULL, NULL, NULL),
(57, 'lang_delete', NULL, NULL, NULL),
(58, 'city_access', NULL, NULL, NULL),
(59, 'city_create', NULL, NULL, NULL),
(60, 'city_edit', NULL, NULL, NULL),
(61, 'city_show', NULL, NULL, NULL),
(62, 'city_delete', NULL, NULL, NULL),
(63, 'facilities_access', NULL, NULL, NULL),
(64, 'facilities_create', NULL, NULL, NULL),
(65, 'facilities_edit', NULL, NULL, NULL),
(66, 'facilities_show', NULL, NULL, NULL),
(67, 'facilities_delete', NULL, NULL, NULL),
(68, 'buildingType_access', NULL, NULL, NULL),
(69, 'buildingType_create', NULL, NULL, NULL),
(70, 'buildingType_edit', NULL, NULL, NULL),
(71, 'buildingType_show', NULL, NULL, NULL),
(72, 'buildingType_delete', NULL, NULL, NULL),
(73, 'subscription_access', NULL, NULL, NULL),
(74, 'subscription_create', NULL, NULL, NULL),
(75, 'subscription_edit', NULL, NULL, NULL),
(76, 'subscription_show', NULL, NULL, NULL),
(77, 'subscription_delete', NULL, NULL, NULL),
(78, 'appUser_access', NULL, NULL, NULL),
(79, 'appUser_edit', NULL, NULL, NULL),
(80, 'owner_access', NULL, NULL, NULL),
(81, 'owner_show', NULL, NULL, NULL),
(82, 'owner_edit', NULL, NULL, NULL),
(83, 'role_access1', NULL, NULL, NULL),
(84, 'branch_access', NULL, NULL, NULL),
(85, 'branch_create', NULL, NULL, NULL),
(86, 'branch_edit', NULL, NULL, NULL),
(87, 'branch_show', NULL, NULL, NULL),
(88, 'branch_delete', NULL, NULL, NULL),
(89, 'offer_access', NULL, NULL, NULL),
(90, 'offer_create', NULL, NULL, NULL),
(91, 'offer_edit', NULL, NULL, NULL),
(92, 'offer_show', NULL, NULL, NULL),
(93, 'offer_delete', NULL, NULL, NULL),
(94, 'appuser_access', NULL, NULL, NULL),
(95, 'appuser_edit', NULL, NULL, NULL),
(96, 'booking_access', NULL, NULL, NULL),
(97, 'booking_edit', NULL, NULL, NULL),
(98, 'employee_access', NULL, NULL, NULL),
(99, 'employee_create', NULL, NULL, NULL),
(100, 'employee_edit', NULL, NULL, NULL),
(101, 'employee_delete', NULL, NULL, NULL),
(102, 'employee_show', NULL, NULL, NULL),
(103, 'custom_notification_access', NULL, NULL, NULL),
(104, 'branch_manager_access', NULL, NULL, NULL),
(105, 'branch_booking_access', NULL, NULL, NULL),
(106, 'branch_employee_access', NULL, NULL, NULL),
(107, 'branch_review_access', NULL, NULL, NULL),
(108, 'review_access', NULL, NULL, NULL),
(109, 'booking_employee_access', NULL, NULL, NULL),
(110, 'review_access', NULL, NULL, NULL),
(111, 'review_access', NULL, NULL, NULL),
(112, 'branch_review_access', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(2, 2),
(2, 4),
(2, 5),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 8),
(1, 10),
(1, 11),
(1, 9),
(1, 38),
(1, 39),
(1, 40),
(3, 7),
(1, 7),
(1, 42),
(1, 43),
(1, 44),
(1, 41),
(4, 7),
(5, 3),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 72),
(1, 73),
(1, 74),
(1, 75),
(1, 76),
(1, 77),
(1, 78),
(1, 79),
(1, 80),
(1, 81),
(1, 82),
(6, 2),
(6, 3),
(6, 4),
(8, 1),
(7, 3),
(7, 5),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 83),
(1, 94),
(1, 95),
(1, 96),
(1, 97),
(1, 98),
(1, 99),
(1, 100),
(1, 101),
(1, 102),
(1, 103),
(9, 104),
(9, 105),
(9, 106),
(9, 107),
(9, 88),
(10, 12),
(10, 13),
(10, 14),
(10, 15),
(10, 17),
(10, 18),
(10, 19),
(10, 20),
(10, 89),
(10, 90),
(10, 91),
(10, 92);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `star` int(11) NOT NULL DEFAULT '0',
  `cmt` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `user_id`, `branch_id`, `booking_id`, `star`, `cmt`, `created_at`, `updated_at`) VALUES
(1, 11, 1, 1, 5, 'good services', '2020-06-04 11:42:59', '2020-06-04 11:42:59');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'Wow', '2020-01-23 06:46:34', '2020-01-24 00:00:29', '2020-01-24 00:00:29'),
(3, 'Finase', '2020-01-29 07:34:27', '2020-06-03 11:20:32', '2020-06-03 11:20:32'),
(4, 'Finance Officer', '2020-02-02 04:45:37', '2020-05-19 22:48:58', '2020-05-19 22:48:58'),
(5, 'Super visor 2', '2020-02-10 04:09:56', '2020-06-03 11:20:38', '2020-06-03 11:20:38'),
(6, 'title', '2020-05-19 22:40:44', '2020-06-03 11:20:42', '2020-06-03 11:20:42'),
(7, 'sda', '2020-05-19 22:52:45', '2020-06-03 11:20:49', '2020-06-03 11:20:49'),
(8, 'xcv', '2020-05-19 22:56:25', '2020-06-03 11:20:57', '2020-06-03 11:20:57'),
(9, 'Branch Manager', '2020-05-25 21:28:00', '2020-05-25 21:28:00', NULL),
(10, 'Data Entry Oprator', '2020-06-03 11:22:05', '2020-06-03 11:22:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 3),
(2, 6),
(2, 5),
(3, 3),
(3, 5),
(3, 7),
(4, 9),
(5, 8),
(6, 8),
(7, 8),
(11, 9),
(12, 9),
(13, 9),
(14, 10),
(15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `type`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'Distance', '35', '2021-01-31 19:00:00', '2021-01-31 19:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `static_notification`
--

CREATE TABLE `static_notification` (
  `id` int(11) NOT NULL,
  `for_what` varchar(255) NOT NULL,
  `title` text NOT NULL,
  `sub_title` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `for_who` int(11) NOT NULL DEFAULT '0' COMMENT '0 = user 1 = provide 2= both',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_notification`
--

INSERT INTO `static_notification` (`id`, `for_what`, `title`, `sub_title`, `status`, `for_who`, `created_at`, `updated_at`) VALUES
(2, 'Approved Booking (User)', '{{farm_title}} Has Accepted Your Booking Request.', 'more information please contact {{owner_name}}.', 1, 0, '2020-01-31 02:10:03', '2020-05-07 04:46:25'),
(3, 'Cancel Booking (Owner - User)', 'Booking For {{farm_title}}  is cancel', 'Your Booking on {{check_in_date}}    is Cancel please many further information {{booking_id}} keep this ref no.', 1, 0, '2020-01-31 00:00:00', '2020-05-07 04:48:20'),
(6, 'Booking Rejected (User)', 'Dear {{user_name}}, {{farm_title}}  Just Reject Booking Req', 'tatatattatata', 1, 0, '0000-00-00 00:00:00', '2020-05-07 04:50:01'),
(7, 'Booking Rejected', 'Booking For {{farm_title}} is rejected', 'Booking From {{user_name}} is rejected on {{rejected_date}}', 1, 2, '2021-02-27 15:45:05', '2021-02-27 15:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT 'default.png',
  `is_best` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '10',
  `description` varchar(255) DEFAULT NULL,
  `for_who` int(11) NOT NULL DEFAULT '0' COMMENT '0 = both 1 = women 2 = male',
  `duration` int(11) NOT NULL DEFAULT '0',
  `preparation_time` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `cat_id`, `icon`, `is_best`, `price`, `description`, `for_who`, `duration`, `preparation_time`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hair Wash Herbal', 1, 'default.png', 1, 30, 'Looking for a quick haircut? This is the one for you.', 0, 20, 10, 1, '2020-06-03 17:22:26', '2021-03-18 23:32:14', '2021-03-18 23:32:14'),
(2, 'Professional Hair Wash', 1, 'default.png', 0, 25, 'Looking for a quick haircut? This is the one for you.', 1, 30, 10, 1, '2020-06-03 17:23:15', '2021-03-18 23:32:08', '2021-03-18 23:32:08'),
(3, 'Hair Spa Wash', 1, 'default.png', 0, 35, 'Looking for a quick haircut? This is the one for you.', 0, 45, 5, 1, '2020-06-03 17:23:57', '2021-03-18 23:32:00', '2021-03-18 23:32:00'),
(4, 'Deep Tissue Massage', 2, 'default.png', 1, 300, 'Slow,firm and specific strokes help to relieve chronic muscle tension and pain,improve mobility and circulation. This massage melts away stress and brings your body back to a relaxed state.', 0, 90, 25, 1, '2020-06-03 17:33:29', '2020-06-03 17:33:29', NULL),
(5, 'Swedish Massage', 2, 'default.png', 0, 600, 'Swedish massage therapy is probably one of the more relaxing and therapeutic massage techniques.', 2, 120, 20, 1, '2020-06-03 17:34:18', '2020-06-03 17:35:32', NULL),
(6, 'Foot Rub', 2, 'default.png', 0, 50, 'Foot reflexology is based on the principle that there are reflex points on the feet that correspond to every organ', 1, 120, 20, 1, '2020-06-03 17:34:53', '2020-06-03 17:35:41', NULL),
(7, 'Brow Shaping', 3, 'default.png', 0, 35, 'We will analyse your bone structure and brow history & determine the best shape.', 1, 15, 5, 1, '2020-06-03 17:38:43', '2020-06-04 12:02:01', NULL),
(8, 'Henna Brow Tinting', 3, 'default.png', 0, 50, 'Henna tinting \'stains\' the skin more effectively & naturally than regular tinting and sets to a powder like matte finish.', 1, 30, 15, 1, '2020-06-03 17:39:27', '2020-06-04 12:02:40', NULL),
(9, 'Custom Tinting', 3, 'default.png', 1, 35, 'Custom Blended for each client to either add thickness, lighten hair, fill in gaps and soften the brows.', 1, 10, 10, 1, '2020-06-03 17:40:20', '2020-06-04 12:03:01', NULL),
(10, 'Test Sub Category 1', 5, 'default.png', 1, 1000, 'No Description', 1, 30, 10, 1, '2021-03-18 23:59:24', '2021-03-19 00:01:24', '2021-03-19 00:01:24'),
(11, 'Test Sub Category 2', 5, 'default.png', 1, 250, 'adsasdasd', 0, 20, 20, 1, '2021-03-19 00:00:03', '2021-03-19 00:01:24', '2021-03-19 00:01:24');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(700) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` decimal(13,9) NOT NULL,
  `tax_amount` decimal(13,9) NOT NULL DEFAULT '0.000000000',
  `status` int(11) NOT NULL DEFAULT '1',
  `booking_id` int(11) NOT NULL,
  `emp_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_id` int(11) NOT NULL,
  `emp_email` int(11) NOT NULL,
  `services` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `service_start_time` datetime NOT NULL,
  `service_end_time` datetime NOT NULL,
  `payment_purpose` int(11) NOT NULL DEFAULT '1' COMMENT '0 =>not payed yet, 1 => In process, 2 => Succeeded, 3 => canceled, 4 => payment failed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payment_intent_id` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Id for creating order payment'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Admin', 'admin@admin.com', '2020-05-19 01:07:15', '$2y$10$tqKuEqlmW1p5FjXG8d03N.wLfJL5hEL2T7M8PDEUOIb3hUWuQ.VxK', 'RVRlC2Yc3lJn4DtzyqRs4Nf9fwrBJ92DhGGAzuAhl3WoFLvhYi6KVkqsjVVV', '2020-05-19 01:07:15', '2020-05-19 01:07:15'),
(8, 'Paloma', 'Paloma@Paloma.com', NULL, '$2y$10$d1bUEAalIuCCrfiaGzcyNe0Cvo.qw.wg2hF5taTnjb237lYfroQh6', NULL, '2020-06-03 12:13:08', '2020-06-03 12:13:08'),
(9, 'Zoia', 'zoia@email.com', NULL, '$2y$10$TZPHeJCRGpHfO4ZgfL00Z..Vq8/JDdVRgICFVPI5io38Tlr87SgRO', NULL, '2020-06-03 12:14:28', '2020-06-03 12:14:28'),
(10, 'Josh', 'Josh@email.com', NULL, '$2y$10$YzSQCvyIjvFVEs9XaTuFau4nsjcdN9V3kGfURYJA.3ufzdl411yGS', NULL, '2020-06-03 12:15:51', '2020-06-03 12:15:51'),
(11, 'James Mason', 'JamesMason@mail.com', NULL, '$2y$10$eZM9HHA6WCbAgai6DRv33.r0wBEAf0Iufja4EV5Mrpu6sCCFYS6CG', NULL, '2020-06-03 12:26:43', '2020-06-03 12:26:43'),
(12, 'Carter Wyatt', 'CarterWyatt@email.com', NULL, '$2y$10$m.ruxYyrV0uz70yKOHHggOz3MstvWT17IkuCsYxs8oY6U4HGLZ3Aa', NULL, '2020-06-03 12:27:30', '2020-06-03 12:27:30'),
(13, 'Julian Grayson', 'JulianGrayson@mail.com', NULL, '$2y$10$aeskuNwJXeY7AxDd81LnfusR0bnP5/sT4cMniPFsSO5yjHn3Agmq.', NULL, '2020-06-03 12:27:58', '2020-06-03 12:27:58'),
(14, 'Data Oprator', 'dataoprator@email.com', NULL, '$2y$10$bN6JLnGmA0fdsJsx3XADz.WIrVN2cr5w1Mkvb8.Ns.zuhFd99Hl/.', NULL, '2020-06-03 12:29:35', '2020-06-03 12:29:35'),
(15, 'Maaz Ur Rehman', 'formanite.yaqoob92@gmail.com', NULL, '$2y$10$bG0u6XfvSDhVWJbnl2KXt.XaWCDtAZ2orDjSTbQ56Q1isk5WurJfa', NULL, '2021-02-16 23:23:25', '2021-02-16 23:23:25'),
(16, 'EMP1', 'emp+100@gmail.com', NULL, '$2y$10$h7ErFItK6xAT7/NzGeqOkOziJVRq7rGa./Nk/JITDGnP1omGdWSfm', NULL, '2021-02-27 11:32:04', '2021-02-27 11:32:04'),
(17, 'Emp User 2', 'employeeuser_2@gmail.com', NULL, '$2y$10$tRDR6E4bImscNeMyPXiS0eh4Le0voTWPHmHRPCF0PvaiBsa9UheCi', NULL, '2021-03-01 08:28:50', '2021-03-01 08:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_sockets`
--

CREATE TABLE `user_sockets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `socket_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `app_role` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `booking_id` text COLLATE utf8mb4_unicode_ci,
  `booking_primary_key` int(11) DEFAULT NULL,
  `longitude` decimal(13,10) DEFAULT NULL,
  `latitude` decimal(13,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_sockets`
--

INSERT INTO `user_sockets` (`id`, `email`, `user_id`, `socket_id`, `app_role`, `status`, `created_at`, `updated_at`, `booking_id`, `booking_primary_key`, `longitude`, `latitude`) VALUES
(190, 'employeeuser_2@gmail.com', 28, 'P-0-H9W0FMFmwIG1AAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(191, 'employeeuser_2@gmail.com', 28, 'B_lodlVPlhLJfE2TAAAD', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(192, 'usamaahm61@gmail.com', 16, 'OBBQGojvF7jSjuAuAAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(193, 'employeeuser_2@gmail.com', 28, 'UnFldrtyCh8Yh18rAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(194, 'employeeuser_2@gmail.com', 28, 'NE1WjR6dxfkgOY9gAAAF', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(195, 'usamaahm61@gmail.com', 16, 'QAajV6mdHQon6yb4AAAD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(196, 'usamaahm61@gmail.com', 16, 'kKQhhpv-2d2IDS3iAAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(197, 'employeeuser_2@gmail.com', 28, 'nkThSsBFp8JbhngUAAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(198, 'employeeuser_2@gmail.com', 28, '_pFB7hunmGqEYCDQAAAD', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(199, 'employeeuser_2@gmail.com', 28, 'eY35iNyy5Oiw9JdtAAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(200, 'employeeuser_2@gmail.com', 28, '0huajeaNSHqhQfrjAAAD', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(201, 'employeeuser_2@gmail.com', 28, '-OT7cisX7j-kKryKAAAF', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(202, 'employeeuser_2@gmail.com', 28, 'RSF17HvHBA2y9SyWAAAH', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(203, 'employeeuser_2@gmail.com', 28, 'hY8TKaFFvJP7Qd_VAAAJ', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(204, 'usamaahm61@gmail.com', 16, 'BVadpnUb-i9ct0eRAAAL', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(205, 'employeeuser_2@gmail.com', 28, 'GJ2MMLLAKpKpGQUxAAAN', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(206, 'usamaahm61@gmail.com', 16, '_Kp9Zsn0TTz_BOiXAAAP', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(207, 'employeeuser_2@gmail.com', 28, 'YqN2uWch5vVXju5-AAAR', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(208, 'employeeuser_2@gmail.com', 28, 'b0_aQ4T7d1VL6PqRAAAT', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(209, 'usamaahm61@gmail.com', 16, 'TNWkzphhw6SPpzSTAAAB', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(210, 'employeeuser_2@gmail.com', 28, 'TzqaW-cCMKh71zsHAAAD', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(211, 'usamaahm61@gmail.com', 16, 'EGCIv4tUiBzTjAKvAAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(212, 'employeeuser_2@gmail.com', 28, 'iM1r-HJJR3lLkbawAAAH', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(213, 'employeeuser_2@gmail.com', 28, '-DUWarmhjV-L0jfEAAAJ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(214, 'employeeuser_2@gmail.com', 28, '2frXb1_e_uqIld_JAAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(215, 'usamaahm61@gmail.com', 16, '8KYP2lM7tQpcl8LkAAAD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(216, 'usamaahm61@gmail.com', 16, 'uRSkgB5WyoYIh_V8AAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(217, 'employeeuser_2@gmail.com', 28, 'LIh1TGX4Uv5s1ApaAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(218, 'usamaahm61@gmail.com', 16, 'NKeQA5IKI9xv_3r2AAAB', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(219, 'usamaahm61@gmail.com', 16, '_uPgeSjYtHTcFfFyAAAD', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(220, 'employeeuser_2@gmail.com', 28, 'a9Cr-3P6ZZ2dNVzDAAAF', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(221, 'employeeuser_2@gmail.com', 28, '1ny5zK_7CogySDVhAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(222, 'employeeuser_2@gmail.com', 28, '0G0d10IGvv2ujgYnAAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(223, 'usamaahm61@gmail.com', 16, 'gZgH-svZwsyrHaA5AAAD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(224, 'usamaahm61@gmail.com', 16, '-T4JwADQxD-Bie46AAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(225, 'employeeuser_2@gmail.com', 28, 'QdVY1bFXUS6fUflRAAAH', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(226, 'employeeuser_2@gmail.com', 28, 'GmzP8uOhFg6Bl5HWAAAJ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(227, 'employeeuser_2@gmail.com', 28, 'lNluSj1ervBYymZBAAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(228, 'usamaahm61@gmail.com', 16, 'yPUjpug-0UI3-nv4AAAD', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(229, 'employeeuser_2@gmail.com', 28, 'V3JXSzREx7n0-xWeAAAF', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(230, 'employeeuser_2@gmail.com', 28, 'k2pW1CZaGXhiEu0FAAAB', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(231, 'usamaahm61@gmail.com', 16, '_VucKT_LKBEIYlbVAAAD', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(232, 'usamaahm61@gmail.com', 16, 'QACh34zOqLW9XmMZAAAB', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(233, 'usamaahm61@gmail.com', 16, 'MX4HieJzL660SN-fAAAD', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(234, 'employeeuser_2@gmail.com', 28, 'tBQ4yduOXmgaBAdNAAAF', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(235, 'employeeuser_2@gmail.com', 28, 'SGvPWleZ9YWcXaY-AAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(236, 'usamaahm61@gmail.com', 16, 'AHOINbJbqENqTWqCAAAD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(237, 'usamaahm61@gmail.com', 16, '1ZRI_D7yHvM8u8vGAAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(238, 'employeeuser_2@gmail.com', 28, 'xujIePxahVkxSN3OAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(239, 'usamaahm61@gmail.com', 16, 'Dw8IRvDlgrdEaMCvAAAC', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(240, 'employeeuser_2@gmail.com', 28, 'UPEeh9klUYAQBAibAAAD', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(241, 'usamaahm61@gmail.com', 16, 'b7D13aw8m1kZwgC0AAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(242, 'employeeuser_2@gmail.com', 28, 'sVdGtPmoC4sfwZNXAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(243, 'usamaahm61@gmail.com', 16, 'FPr6Ces8PZ2BIcQ7AAAB', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(244, 'employeeuser_2@gmail.com', 28, 'lgX78nJDdriYrtcHAAAD', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(245, 'usamaahm61@gmail.com', 16, 'WOG0mnCaY-CTm45jAAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(246, 'employeeuser_2@gmail.com', 28, 'UlEj7CyU4hIpoq08AAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(247, 'usamaahm61@gmail.com', 16, 'BELKza-D1XzpjZibAAAB', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(248, 'employeeuser_2@gmail.com', 28, 'gjdNpq8Z0Wti7VOkAAAD', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(249, 'usamaahm61@gmail.com', 16, 'TGWQVbgZvCWABVS8AAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(250, 'employeeuser_2@gmail.com', 28, 'd4MJegQPANzFnAdVAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(251, 'usamaahm61@gmail.com', 16, 'UBNJeFeAY8rALN1gAAAB', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(252, 'usamaahm61@gmail.com', 16, 'sRaG5u-Tl9FyjEO-AAAD', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(253, 'employeeuser_2@gmail.com', 28, '0UUBfGxwOhiyfrt7AAAF', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(254, 'employeeuser_2@gmail.com', 28, '58OCHzVGznh6TiMXAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(255, 'employeeuser_2@gmail.com', 28, 'EzSO_77_RFSoSvOoAAAB', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(256, 'usamaahm61@gmail.com', 16, 'xK0rNRXYWmKMv_E5AAAD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(257, 'usamaahm61@gmail.com', 16, 'Z1s-vRhc3F2nB7zOAAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(258, 'employeeuser_2@gmail.com', 28, 'TmqLfboEHb3ZHXGbAAAB', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(259, 'usamaahm61@gmail.com', 16, 'xctPW_zJ-cHOhFiEAAAD', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(260, 'employeeuser_2@gmail.com', 28, 'rURfxFbAcFnMHIe-AAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(261, 'usamaahm61@gmail.com', 16, 'zZc4yBTDHWuifD7_AAAD', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(262, 'usamaahm61@gmail.com', 16, 'SKNlxVVyUa3RmyngAAAF', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(263, 'employeeuser_2@gmail.com', 28, 'UPLVd_NMvUR7IazsAAAH', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(264, 'employeeuser_2@gmail.com', 28, 'Gfs2w4dijMifs1YrAAAJ', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(265, 'employeeuser_2@gmail.com', 28, 'ASCeADNnJ16HLMfHAAAL', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(266, 'employeeuser_2@gmail.com', 28, 'MKjrXm8wJj3ojSamAAAN', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(267, 'employeeuser_2@gmail.com', 28, '47n-St0PrrrM8_jlAAAP', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(268, 'employeeuser_2@gmail.com', 28, 'eejnoDLuXy3JWCduAAAB', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(269, 'usamaahm61@gmail.com', 16, 'HFmPGEYQvC6vKUP-AAAD', 1, 1, NULL, NULL, NULL, NULL, '74.2533000000', '31.4728000000'),
(270, 'employeeuser_2@gmail.com', 28, 'E-vLFUgvihuGd_F5AAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(271, 'usamaahm61@gmail.com', 16, 'VA5OzWvZ0sa-YnJUAAAD', 1, 0, NULL, NULL, NULL, NULL, '74.2533000000', '31.4728000000'),
(272, 'usamaahm61@gmail.com', 16, 'WfJbx4uzCpM55C0DAAAF', 1, 1, NULL, NULL, NULL, NULL, '74.2533000000', '31.4728000000'),
(273, 'employeeuser_2@gmail.com', 28, 'bMHzV4sE7pkLaFOlAAAH', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(274, 'employeeuser_2@gmail.com', 28, 'OHzcxbexGB8aEIEaAAAB', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(275, 'usamaahm61@gmail.com', 16, 'LevC3vMJt9OFeSLDAAAD', 1, 1, NULL, NULL, NULL, NULL, '74.2533000000', '31.4728000000'),
(276, 'employeeuser_2@gmail.com', 28, '5-bYYedkehLDeuZGAAAF', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_setting`
--
ALTER TABLE `admin_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`,`password`,`status`,`otp`,`verified`,`app_role`,`pin_code`,`unique_identification_code`,`admin_approval`);

--
-- Indexes for table `auth_info`
--
ALTER TABLE `auth_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_child`
--
ALTER TABLE `booking_child`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`,`service_id`,`emp_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `booking_master`
--
ALTER TABLE `booking_master`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`branch_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `offer_id` (`offer_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cart_email_unique` (`email`),
  ADD UNIQUE KEY `cart_user_id_unique` (`user_id`);

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `new_unique_identifier` (`email`,`user_id`,`service_id`,`cart_id`,`cat_id`,`checkout`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `checkout_email_unique` (`email`),
  ADD UNIQUE KEY `checkout_user_id_unique` (`user_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_detail`
--
ALTER TABLE `employee_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `emp_jobs`
--
ALTER TABLE `emp_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `new_unique_identifier_emp_jobs` (`employee_email`,`employee_user_id`,`booking_id`,`user_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `job_location_history`
--
ALTER TABLE `job_location_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_indexing` (`sender_user_id`,`sender_user_email`,`reciever_user_id`,`reciever_user_email`,`type`);

--
-- Indexes for table `notification_tbl`
--
ALTER TABLE `notification_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payment_method_code` (`payment_method_code`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_476162` (`role_id`),
  ADD KEY `permission_id_fk_476162` (`permission_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`booking_id`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_476171` (`user_id`),
  ADD KEY `role_id_fk_476171` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_notification`
--
ALTER TABLE `static_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_sockets`
--
ALTER TABLE `user_sockets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_setting`
--
ALTER TABLE `admin_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `auth_info`
--
ALTER TABLE `auth_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `booking_child`
--
ALTER TABLE `booking_child`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `booking_master`
--
ALTER TABLE `booking_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `employee_detail`
--
ALTER TABLE `employee_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `emp_jobs`
--
ALTER TABLE `emp_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_location_history`
--
ALTER TABLE `job_location_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notification_tbl`
--
ALTER TABLE `notification_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `static_notification`
--
ALTER TABLE `static_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user_sockets`
--
ALTER TABLE `user_sockets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking_child`
--
ALTER TABLE `booking_child`
  ADD CONSTRAINT `booking_child_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `booking_master` (`id`),
  ADD CONSTRAINT `booking_child_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `booking_master`
--
ALTER TABLE `booking_master`
  ADD CONSTRAINT `booking_master_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `app_users` (`id`),
  ADD CONSTRAINT `booking_master_ibfk_2` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `booking_master_ibfk_3` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_detail`
--
ALTER TABLE `employee_detail`
  ADD CONSTRAINT `employee_detail_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `app_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`booking_id`) REFERENCES `booking_master` (`id`),
  ADD CONSTRAINT `review_ibfk_3` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`);

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
