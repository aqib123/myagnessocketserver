var appUserModal = require('./models/appUser');
var socketUsersModal = require('./models/socketUsers');
var bookingMasterModal = require('./models/bookingMaster.js');
var bookingChildModal = require('./models/bookingChild.js');
var bookingMaster = require('./routes/booking.js');
var empJobsModal = require('./models/empJobs.js');
var empJobLocationHIstoryModal = require('./models/empJobLocationHistory.js');
var request = require('request');

module.exports = function(server, app, io) {

	// 1) Authenticate Socket from Given Token
	io.use(function(socket, next) {
	  if (socket.handshake.query && socket.handshake.query.token) {
	  	// Verify Token
	  	appUserModal.verifyToken(socket.handshake.query.token, function(err, result) {
	  		console.log('Error = ', err) 
	  		console.log('Result = ', result)
	  		if (err || !result) next(new Error('Authentication error'));
		    if (result) {
		    	// Save Active Record in Socket Table to retrieve
		    	socketUsersModal.saveSocketDetails(result, socket.id, function(err, socketResult) {
		    		console.log('Socket Result Error = ' + err)
		    		console.log('Socket Result saved ='+ socketResult)
		    		if (err || !socketResult) next(new Error('Some Server Error while data processing'));
		    		else next()
		    	})
		    }
		    else {
	    		next(new Error('Authentication error'));
		    }
	  	})
	  }
	  else {
	    next(new Error('Authentication error Outer'));
	  }    
	}).on('connection', function(socket) {
	    console.log('Inside secure connection')
	    // TODO: Insert Socket Id and User details on Socket Table
	    socket.on('message', function(message) {
	    	console.log('SocketId = '+socket.id)

	    	listner(message, socket, io)
	    })

	    socket.on('disconnect', () => {
	    	console.log('here in disconnect')
		    socketUsersModal.deactivateSocketStatus(socket.id, function(err, socketResult){
		    	console.log(err)
		    	console.log(socketResult)
		    	// if (err || !socketResult) 
		    })
		  })
	})

	// 2) Listner of Socket Server
	function listner(message, socket, io) {

		// Get Socket Details
		socketUsersModal.getSocketUserDetails(socket.id, function(err, socketResult) {
			if (err || !socketResult) socket.emit('message', {code: err, data: {}})
			else {
				// socket.emit('message', {'socket_id': socket.id})
				switch(message.type) {

					// Case 1 (For Starting a Job if not started Yet)
				  case 1:
				  	// TODO: 1) Check if Booking valid and still active (Need to test)
				  	// TODO: 2) Check if its same day and dateTime is not greater then startTime (Need to test)
				  	// TODO: 3) Update Status of Employee starting the starting the job (Active) (Need to test)
				  	if (socketResult[0].app_role != 2) socket.emit('message', {code: 5008, data: {}})
				  	else if (!message.data.bookingId) socket.emit('message', {code: 5001, data: {}})	
				  	else {
				  		bookingChildModal.bookingChildRecord(socketResult[0].user_id, message.data.bookingId, function(err, bookingRecord) {
					  		if (err || !bookingRecord) socket.emit('message', {code: err, data: {}})
					  		else {
					  			let currentDate = new Date()
					  			console.log('bookingRecord.start_time')
					  			console.log(bookingRecord[0].start_time)
					  			console.log(new Date(bookingRecord[0].start_time))
					  			let startDate = new Date(bookingRecord[0].start_time);
					  			console.log ('Current Date = ' +currentDate)
					  			console.log ('Start Date = ' +startDate)
					  			// if (currentDate.setHours(0,0,0,0) != startDate.setHours(0,0,0,0)) socket.emit('message', {code: 5028, data: {}})
					  			// else {
					  				// Create or First to check if Job already Exist
				  					empJobsModal.createEmpJob(socket.id, bookingRecord, socketResult, function (err, jobRecord){
				  						if (err || !jobRecord) socket.emit('message', {code: err, data: {}})
				  						else {
				  							bookingChildModal.updateStatus(1, socketResult[0].user_id, bookingRecord[0].booking_id, function(err, updatedBooking) {
				  								if (err || !updatedBooking) socket.emit('message', {code: err, data: {}})
				  								// Successfully created a job
				  								request.post({
												  headers: {'content-type' : 'application/json'},
												  json: true,
												  url:     'http://prodigousintl.com/api/send/notification',
												  body:    {'recieverEmail': bookingRecord[0].user_email, 'recieverName': bookingRecord[0].user_email, 'senderEmail': bookingRecord[0].emp_email, 'senderName': bookingRecord[0].emp_email, 'bookingId': bookingRecord[0].booking_id, type: 6}
												}, function(error, response, body){
												  console.log(body);
												});
				  								socket.emit('message', {code: 5006, data: {jobId: jobRecord.insertId}})
				  							})
				  						}
				  					})
					  			// } 
					  		}
					  	})
				  	} 
				    break;

				  // Case 2 (For updating the Current Location and Sending the location to client)  
				  case 2:
				    // 1) Get Geolocation from employee
				    // 2) Save Location data into In Memory Cache DB
				    // 3) Check if user is active on socket send him the updated location
				    // 4) Check if user is not active update the notification to notify him
				    if (socketResult[0].app_role != 2) socket.emit('message', {code: 5008, data: {}})
				    // else if (!message.data.jobId) socket.emit('message', {code: 5007, data: {}})
				    else if (!message.data.currentLatitude) socket.emit('message', {code: 5009, data: {}})
				    else if (!message.data.currentLongitude) socket.emit('message', {code: 5010, data: {}})
				    // else if (!message.data.targetLatitude) socket.emit('message', {code: 5011, data: {}})
				    // else if (!message.data.targetLongitdue) socket.emit('message', {code: 5012, data: {}})
				    else {
				    	empJobsModal.getAllEmpJobsDetails(socketResult[0].user_id, function (err, jobResult) {
				    		console.log('Error = '+err)
				    		console.log('jobResult = '+jobResult)
				    		if (err || !jobResult) socket.emit('message', {code: 5022, data: {}})
				    		// else if (jobResult[0].status == 2) socket.emit('message', {code: 5013, data: {}})
				    		// else if (jobResult[0].status == 3) socket.emit('message', {code: 5014, data: {}})
				    		// else if (jobResult[0].status == 4) socket.emit('message', {code: 5015, data: {}})
				    		else {
				    			// Check if started location and target location is not updated please update first
				    			empJobsModal.updateEmpJob(message.data.currentLatitude, message.data.currentLongitude, socketResult[0].email, function (err, empJobUpdated){
				    				console.log('ErrorupdateEmpJob = '+err)
				    				console.log('empJobUpdated = '+empJobUpdated)
				    				if (err || !empJobUpdated) socket.emit('message', {code: err, data: {}})
				    				else {
				    					// Update Location in Job Location History
				    					// empJobLocationHIstoryModal.createLocationHistory(message.data, jobResult, function (err, locationUpdated) {
				    					// 	if (err || !locationUpdated) socket.emit('message', {code: 5017, data: {}})
				    					// 	else {
				    							// Get Socket Detail of Active user
				    							console.log(jobResult[0].user_id)
													socketUsersModal.getSocketUserDetailsFromGivenUserDetails(jobResult, function(err, socketClientResult) {
														if (err === 5018) {
															// User is Not active Send FCM Notification on his Mobile for Once.
														} else {
															// Send Location Updated Successfull message to Active User
															socketClientResult.forEach(function(clientRecord, key){
																jobResult.filter((job) => {
																	if(job.user_id == clientRecord.user_id) {
																		// console.log(filterJobRecord)

																		console.log(clientRecord.socket_id)
																		let locationData = {jobId: job.id, currentLat: message.data.currentLatitude, currentLong: message.data.currentLongitude, targetLat: clientRecord.latitude, targetLong: clientRecord.longitude, bookingId: job.booking_id, workerEmail: job.employee_email, branchName: job.branch_name}
																		// let locationData = {bookingId: job.booking_id, workerEmail: job.employee_email, branchName: job.branch_name}
																		io.to(clientRecord.socket_id).emit('message', {code: 5020, data: locationData})
																		socket.emit('message', {code: 5021, data: {}})
																	}
																})
															})
														} 
													})
				    					// 	}
				    					// })
				    				}
				    			})
				    		}
				    	})
				    }		
				    break;

				    // Case 3 (Reached at the Target Place)
				    case 3:
				    // 1) Check if job is already started
				    // 2) Cehck if job is not cancelled or already reached
				    if (socketResult[0].app_role != 2) socket.emit('message', {code: 5008, data: {}})
				    else if (!message.data.jobId) socket.emit('message', {code: 5007, data: {}})
				    else {
				    	empJobsModal.getEmpJobsDetails(message.data.jobId, socketResult[0].user_id, function (err, jobResult) {
				    		if (err || !jobResult) socket.emit('message', {code: 5022, data: {}})
				    		else if (jobResult[0].status == 2) socket.emit('message', {code: 5013, data: {}})
				    		else if (jobResult[0].status == 3) socket.emit('message', {code: 5014, data: {}})
				    		else if (jobResult[0].status == 4) socket.emit('message', {code: 5015, data: {}})
				    		else {
				    			empJobsModal.updateEmpJobStatus(message.data.jobId, 'reached_at', 2, socketResult[0].user_id, function (err, empJobUpdated) {
				    				if (err || !empJobUpdated) socket.emit('message', {code: 5023, data: {}})
				    				else {
				    					bookingChildModal.updateStatus(2, socketResult[0].user_id, jobResult[0].booking_id, function(err, updatedBooking) {
			  								if (err || !updatedBooking) socket.emit('message', {code: err, data: {}})
			  									console.log ('User ID = '+jobResult[0].user_id)
			  								socketUsersModal.getSocketUserDetailsFromGivenUser(jobResult[0].user_id, function(err, socketClientResult) {
			  									console.log (socketClientResult)
			  									if (socketClientResult) {
			  										io.to(socketClientResult[0].socket_id).emit('message', {code: 5024, data: {jobId: message.data.jobId}})
			  									}
			  									request.post({
												  headers: {'content-type' : 'application/json'},
												  json: true,
												  url:     'http://prodigousintl.com/api/send/notification',
												  body:    {'recieverEmail': jobResult[0].user_email, 'recieverName': jobResult[0].user_email, 'senderEmail': jobResult[0].employee_email, 'senderName': jobResult[0].employee_email, 'bookingId': jobResult[0].booking_id, type: 2}
												}, function(error, response, body){
												  console.log(body);
												});
			  									socket.emit('message', {code: 5025, data: {}})
					    					})
			  							})
				    				}	
				    			})
				    		}
				    	})
				    }
				  default:
				    // code block
				}
			}	
		})
	}

}