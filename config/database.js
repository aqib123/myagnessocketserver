// This is an example database config file. Replace these presets with your database info and add this file to your .gitignore.
module.exports = {
  'connection': {
    'host': 'localhost',
    'user': 'root',
    'password': 'root',
    'port': 8889,
    'connectionLimit' : 10,
    'database' : 'myagness_latest'
  },
  'database': 'myagness_latest'
}
