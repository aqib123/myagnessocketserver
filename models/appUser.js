var db     = require('./db');
// var messages = require('../config/messageTypes.js');

// 1) Token Verification
var verifyToken = function(token, callback) {
  console.log('Verify Token') 	
  // Check if there's already a user with that email
  db.query('SELECT * FROM app_users WHERE token = ?', [token], function(err, rows) {
    console.log(err)
    if (err)
      return callback(5000);
  	console.log(rows.length)
    if (rows.length > 0) {
      return callback(null, rows);
    } else {
      // No user exists, create the user
      return callback(5000)
    }
  });
}

exports.verifyToken = verifyToken;