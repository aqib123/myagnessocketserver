let db = require('./db');

// 1) Get record from Booking Chile
let bookingChildRecord = function (empId, bookingId, callback) {
	console.log(new Date().toISOString().slice(0, 19).replace('T', ' '));
	db.query('SELECT * FROM booking_child WHERE booking_id = ? AND emp_id = ?', [bookingId, empId], function(err, rows) {
		console.log('here')
		console.log(err)
		console.log(rows)
	    if (err) return callback(5003);
	    if (rows.length > 0) {
	      return callback(null, rows);
	    } else {
	      return callback(5003)
	    }
	});
}

// 2) Update Status of Booking
let updateStatus = function (status, empId, bookingId, callback) {
	console.log('Update Child Record status')
	db.query('UPDATE booking_child SET status = ? WHERE emp_id = ? AND booking_id = ?',[status, empId, bookingId], function (err, updatedRows) {
		if (err) {
	        return callback(5027);
	    }
	    // Successfully socket updated
	    return callback(null, true);
	})
}

exports.bookingChildRecord = bookingChildRecord
exports.updateStatus = updateStatus