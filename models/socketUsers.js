var db     = require('./db');

// 1) Save socket details in Database
var saveSocketDetails = function(record, socketId, callback) {
  // Check if there's already a user with that email
  db.query('INSERT INTO user_sockets ( user_id, email, socket_id, app_role, status, longitude, latitude) values (?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE socket_id = ?, status = ?',
    [record[0].id, record[0].email, socketId, record[0].app_role,1, record[0].longitude, record[0].latitude, socketId, 1],
    function(err) {
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          return callback(null, true)
        }
        return callback(err);
      }
      // Successfully socket added
      return callback(null, true);
    }
  );
}

var deactivateSocketStatus = function (socketId, callback) {
  console.log('deactivateSocketStatus sock ='+ socketId)
  db.query('UPDATE user_sockets SET status = 0 WHERE socket_id = ?',
    [socketId],
    function(err) {
      if (err) {
        return callback(err);
      }
      // Successfully socket updated
      return callback(null, true);
    }
  );
}

var getSocketUserDetails = function (socketId, callback) {
  db.query('SELECT * FROM user_sockets WHERE socket_id = ? AND status = ?', [socketId, 1], function(err, rows) {
    if (err)
      return callback(5002);
    if (rows.length > 0) {
      return callback(null, rows);
    } else {
      return callback(5002)
    }
  });
}

var getSocketUserDetailsFromGivenUser = function (userId, callback) {
  db.query('SELECT * FROM user_sockets WHERE user_id = ? AND status = 1', [userId, 1], function(err, rows) {
    if (err)
      return callback(5018);
    if (rows.length > 0) {
      return callback(null, rows);
    } else {
      return callback(5018)
    }
  });
}

var getSocketUserDetailsFromGivenUserDetails = function (socketUsers, callback) {
  let userArray = '('
  let count = 1
  socketUsers.forEach(function(record, key){
    userArray = userArray+record.user_id
    if (socketUsers.length != count )
    {
      userArray = userArray + ','
    }
    count++
  })
  userArray = userArray+')'
  console.log(userArray)
  db.query('SELECT * FROM user_sockets WHERE user_id In '+userArray+' AND status = 1', [], function(err, rows) {
    console.log(err)
    console.log(rows)
    if (err)
      return callback(5018);
    if (rows.length > 0) {
      return callback(null, rows);
    } else {
      return callback(5018)
    }
  });
}

exports.saveSocketDetails = saveSocketDetails;
exports.deactivateSocketStatus = deactivateSocketStatus;
exports.getSocketUserDetails = getSocketUserDetails;
exports.getSocketUserDetailsFromGivenUser = getSocketUserDetailsFromGivenUser;
exports.getSocketUserDetailsFromGivenUserDetails = getSocketUserDetailsFromGivenUserDetails;