var db     = require('./db');

// 1) Insert Jobs Record
let createEmpJob = function (socketId, bookingRecord, socketRecord, callback) {
	// Check if there's already a user with that email
  db.query('INSERT INTO emp_jobs ( employee_user_id, employee_email, branch_id, branch_name, booking_id, user_id, user_email, job_started_at) values (?,?,?,?,?,?,?,?)',
    [bookingRecord[0].emp_id, bookingRecord[0].emp_email, bookingRecord[0].branch_id, bookingRecord[0].branch_name, bookingRecord[0].booking_id, bookingRecord[0].user_id, bookingRecord[0].user_email, new Date().toISOString().slice(0, 19).replace('T', ' ')],
    function(err, result) {
      console.log(err)
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          return callback(5004)
        }
        return callback(5005);
      }
      // Successfully socket added
      return callback(null, result);
    }
  );
}

// 2) Get Jobs Detail
let getEmpJobsDetails = function (jobId, userId, callback) {
  db.query('SELECT * FROM emp_jobs WHERE id = ? AND employee_user_id = ?', [jobId, userId], function(err, rows) {
    console.log(err)
    if (err)
      return callback(5002);
    if (rows.length > 0) {
      return callback(null, rows);
    } else {
      return callback(5002)
    }
  });
}

// 3) Update Emp Job Location
let updateEmpJob = function (currLat, currLon, emp_email, callback) {
	if (!currLat || !currLon) {
		db.query('UPDATE emp_jobs SET started_longitude = ?, started_latitude = ? WHERE employee_email = ? AND status = 1',
	    [currLon, currLat, emp_email],
	    function(err) {
	      if (err) {
	        return callback(5016);
	      }
	      // Successfully socket updated
	      return callback(null, true);
	    }
	  );
	} else {
		return callback(null, true);
	}
}

// 4) Update Emp Job Status
let updateEmpJobStatus = function (jobId, dateColumn, status, userId, callback) {
	db.query('UPDATE emp_jobs SET status = ?, '+dateColumn+' = ? WHERE id = ? AND employee_user_id = ?',
    [status, new Date().toISOString().slice(0, 19).replace('T', ' '), jobId, userId],
    function(err) {
    	console.log(err)
      if (err) {
        return callback(err);
      }
      // Successfully socket updated
      return callback(null, true);
    }
  );
}

// 5) Get All Jobs started Detail for Given Worker
let getAllEmpJobsDetails = function (userId, callback) {
  db.query('SELECT * FROM emp_jobs WHERE status = 1 AND employee_user_id = ?', [userId], function(err, rows) {
    console.log(err)
    if (err)
      return callback(5002);
    if (rows.length > 0) {
      return callback(null, rows);
    } else {
      return callback(5002)
    }
  });
}

exports.createEmpJob = createEmpJob
exports.getEmpJobsDetails = getEmpJobsDetails
exports.updateEmpJob = updateEmpJob
exports.updateEmpJobStatus = updateEmpJobStatus
exports.getAllEmpJobsDetails = getAllEmpJobsDetails