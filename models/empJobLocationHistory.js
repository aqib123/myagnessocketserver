var db     = require('./db');

// 1) Update Location of the Employee reaching to Target location
let createLocationHistory = function (locationData, jobRecord, callback) {
  let values = ''
  let count = 1
  jobRecord.forEach(function(record, key){
    values = values+'('+record.id+','+values.employee_user_id+','+values.branch_id+','+ values.branch_name+','+values.booking_id+','+values.user_id+','+values.user_email+','+values.user_name+','+locationData.currentLongitude+','+locationData.currentLatitude+')'
    if (jobRecord.length != count )
    {
      values = values + ','
    }
  })
	db.query('INSERT INTO job_location_history ( job_id, employee_user_id, branch_id, branch_name, booking_id, user_id, user_email, user_name, longitude, latitude) values ' + values,
    [],
    function(err, result) {
    	console.log('commig erro in location his')
    	console.log(err)
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          return callback(err)
        }
        return callback(err);
      }
      // Successfully socket added
      return callback(null, result);
    }
  );
}

exports.createLocationHistory = createLocationHistory;
